#include"Socket.hpp"
#include"Log.hpp"
#include"Protocol.hpp"
#include<iostream>
#include<string>
void Usage()
{
    std::cout<<"\nPlease use like this:   ./client ServerIP  ServerPort\n"<<std::endl;
}
int main(int argc,const char*argv[])
{
    if(argc!=3)
    {
        Usage();
        exit(11);
    }
    while(true)
    {
        std::cout<<"您是否需要继续进行计算服务(退出填quit,如果不，请随意填写并按回车):";
        std::string s;
        std::getline(std::cin,s);
        //std::cout<<"cin is good?"<<(std::cin.good())<<std::endl;
        if(s=="quit")
        {
            break;
        }
        Socket client;
        Request ask;
        //std::cout<<s<<std::endl;
        std::cout<<"请输入算式：";
        std::cin>>ask.a_>>ask.op_>>ask.b_;
        std::cin.get();//去除最后回车时输入的\n，以免下次上面的getline输入的时候他直接将\n读进去，我们反而输入不了内容
        std::string m(ask.Serialization());
        std::cout<<m<<std::endl;
        client.Connect(argv[1],argv[2]);
       int _send=Send(client.socket_,m);
        if(_send==-1)
        {
            message(ERROR,"错误码：%d  客户端向服务器send失败:%s",errno,strerror(errno));
            continue;
        }
        message(NORMAL,"客户端向服务器send成功");
        int state;
        std::string ret=Recv(client.socket_,&state);
        if(state==1)//接收失败
        {
             message(ERROR,"错误码：%d  客户端recv服务器消息失败:%s",errno,strerror(errno));
              close(client.socket_);
            continue;
        }
        else if(state==2)//对方关闭了用于服务的套接字
        {
             message(ERROR,"客户端尝试从服务器获取信息，但对方断开连接");
              close(client.socket_);
            continue;
        }
        else if(state==0)//接收成功
        {
            message(NORMAL,"客户端从服务器获取信息recv成功");
            std::cout<<"客户端从服务器获取信息recv成功"<<std::endl;
            Response res;
            if(!res.Deserialization(ret))
            {
                 message(ERROR, "服务器发送给客户端的是未遵守协议的字符串");
                  close(client.socket_);
                 continue;
            }
            if(res.state_code==-1)
            {
                message(ERROR,"错误的请求");
                std::cout<<"结果：错误的算法"<<std::endl;
                 close(client.socket_);
                continue;
            }
            std::cout<<"结果是:"<<res.ret_<<std::endl;
            close(client.socket_);
        }
        
        
    }
    return 0;
}