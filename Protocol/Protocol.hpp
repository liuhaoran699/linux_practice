#pragma once
#include <jsoncpp/json/json.h> //jsoncpp在/usr/include这个目录下，标准头文件查找会在/user/include这个目录下进行查找，但是json自己它有自己的头文件路径，所以我们需要明确写出
                               //我们要的头文件是在jsoncpp下的哪一个头文件
#include "Log.hpp"
#include "Socket.hpp"
#include <string>
#define SPACE " "
#define SEP "\r\n"
#define SEPSIZE strlen(SEP)
// 客户端发起的是计算器的请求
// 序列化与反序列化，序列化：将类对象中的数据通过约定好的方式转换为相应格式的字符串然后通过网络传输过去
// 反序列化：将通过网络得到的字符串，按照客户端与服务端约定好的协议再转换为对应的类对象
// 我们虽然也可以直接发送结构化的数据过去，但是这种方式可扩展性和可维护性太差了，所以一般我们不这么做，
// 我们一般在应用层是把结构化的数据转成字节流(粗浅理解为字符串)然后发送给对方，对方基于它所收到的字节流(字符串)，然后对其做反序列化，拿到原来的结构。
// TCP是面向字节流的，可能客户端给服务端连续发送了很多个请求，但在服务端读取这些请求的时候，有可能一下就全部被服务端给读取完了

// 它不像UDP，它是面向数据报,是一个请求一个请求的方式去进行读取，当客户端发送给服务端请求的时候，服务端调用recvfrom来接收请求，那么他读取到的就是完整的报文，
// 那么对于TCP，在调用recv的时候，他是怎么保证读取到的是一个完整的请求呢？它不能够保证
// 单纯的recv是无法解决这个问题的，我们需要进一步定制协议
// 比如这里我们实现的计算服务仅仅是使用 _a _op _b 三者之间用空格分隔是不够的，不能够保证做到读取时读取到的是一个完整报文
// 我们可以加以改良：length\r\na_ _op _b\r\n  //这里的length表示的是报文本身的长度，通过读取该lenth来得知报文的长度从而读取到一个完整的报文，而通过lenth后面的\r和\n，我们可以判定
// 或者约定，在这个\r\n之前的数据就表示报文的长度，也就是说当我们读取到这个\r\n的时候我们就能够确定在这之前读取到的内容表示的是报文的长度，从而能够确定一个完整报文的大小，从而读到一个
// 完整的报文，那么我们可以定制协议为 a_ _op _b\r\n，然后规定读到\r\n就是读取到一个完整的报文，这样行吗？答案是，在这里是可以的，但是在其他的场景下,比如报文的内容中含有\r\n,这样就不可行
// 了。length\r\na_ _op _b\r\n，所以最好还是采用leng\r\n的形式，当然这里我们所采用的分隔区分符是\r\n，我们也可以采用其他的特殊符号作为分隔符，比如\a\b，但是建议还是采用\r\n的形式
// 作为分隔符，因为这样用\r\n做分隔符的可读性非常好。那么length\r\na_ _op _b\r\n ，这里最后的\r\n的作用是什么呢？作用仅仅是是为了增加协议的可读性，加上与不加上都不影响使用，但是最好还是加上
// 注意：上面说的\r\n不参与报文长度的计算，\r\n仅仅是作为特殊字符来作为分隔符使用
// TCP和UDP是传输控制协议
// 我们调用的read/recv,write/send这些IO接口，本质都是拷贝函数，当调用write/send来利用网络发送信息，实际上并不是直接发送给对方，它只是把我们准备要写入发送的数据，从我们定义的缓冲区中
// 拷贝到了发送缓冲区，当调用read/recv的时候，其实也不是直接从对方那里去拿数据，数据其实已经在我们这方的接收缓冲区中了，它只是把接收缓冲区中的内容拷贝到上层我们用户所定义的缓冲区中
// 对于TCP而言(这里不讨论UDP,因为UDP发送和接收的都是完整的报文，不存在后面说的问题)，如果发送的请求被累计在发送缓冲区中，
// 那么在真正要发送请求的时候，有可能发送过去的不是一个完整独立的报文(可能是多个报文，或者几个半...),所以对方在读取的时候就不能保证是一个完整的报文
// 并且我们也可以知道，在TCP下发送数据的次数(这里指的是调用write/recv这些发送接口的次数)和接收数据的次数(这里指的是调用read/send这些接收数据接口的次数)之间没有任何关系，因为
// 我调用许多次发送数据的接口，但是如果这些发送的数据只是被堆积到发送缓冲区中，当下一次调用发送数据的接口时，这些数据才一起被真正发送过去，对方在接收这些数据时，可能一次就给你全读完了(或者读了多次)
// 或者，我们调用一次发送数据的接口，这些数据就被真正发送过去了，但是对方读取的时候可能读取多次才读取完。
// 以上便能够说明问题。
// 那么如果是这样，什么时候进行数据的发送呢？我们在应用层不用管数据的发送时间，，这是由TCP协议决定的

// 目前我们学习到的服务器全部都是前台进程，全都是在前台运行的
// 一个进程是否为前台进程也就是该进程能否获取我们的输入，能否，把我们输入的内容进行处理。几乎所有的情况下，bash都是前台进程

// 声明
int Send(int socket_, const std::string &s);
std::string Recv(int socket_, int *state);
std::string Encode(const std::string &s);
std::string Decode(const std::string &s);

class Request
{
public:
    Request()
    {
    }
    Request(int a, char op, int b) : a_(a), b_(b), op_(op)
    {
    }
    std::string Serialization() // lenth\r\na_ op_ b_\r\n  //使用左边所说的格式进行数据的序列化
    {
#ifdef SELF
        std::string s = std::to_string(a_);
        s += SPACE;
        s += op_;
        s += SPACE;
        s += std::to_string(b_);
#else
        Json::Value root;
        root["first"] = a_;
        root["second"] = b_;
        root["operator"] = op_;
        Json::FastWriter writer;
        std::string s = writer.write(root);
#endif
        return s;
    }
    bool Deserialization(const std::string &s)
    {
#ifdef SELF
        int left = s.find(SPACE);
        if (left == std::string::npos)
        {
            return false;
        }
        int right = s.rfind(SPACE);
        if (right == left) // 说明这有一个分隔符,但实际协议要求的是有两个分隔符
        {
            return false;
        }
        a_ = std::stoi(s.substr(0, left));
        b_ = std::stoi(s.substr(right + 1));
        op_ = s[left + 1];
        return true;
#else
        Json::Value root;
        Json::Reader reader;
        if (!reader.parse(s, root))
        {
            return false;
        }
        a_ = root["first"].asInt();
        b_ = root["second"].asInt();
        op_ = root["operator"].asInt();
        return true;

#endif
    }

public:
    int a_;
    int b_;
    char op_;
};
class Response
{
public:
    Response()
    {
    }
    Response(int ret, int code = 0) : state_code(code), ret_(ret)
    {
    }
    std::string Serialization()
    {
#ifdef SELF
        std::string s = std::to_string(state_code);
        s += SPACE;
        s += std::to_string(ret_);
#else
        Json::Value root;
        root["statecode"] = state_code;
        root["result"] = ret_;
        Json::FastWriter writer;
        std::string s = writer.write(root);
#endif
        return s;
    }
    bool Deserialization(const std::string &s)
    {
#ifdef SELF
        int space = s.find(SPACE);
        if (space == std::string::npos)
        {
            return false;
        }
        state_code = std::stoi(s.substr(0, space));
        ret_ = std::stoi(s.substr(space + 1));
        return true;
#else
        Json::Value root;
        Json::Reader reader;
        if (!reader.parse(s, root))
        {
            return false;
        }
        state_code = root["statecode"].asInt();
        ret_ = root["result"].asInt();
        return true;
#endif
    }

public:
    int state_code;
    int ret_;
};
void calculate(int serversocket, const std::string info)
{
    // 反序列化
    Request ask;
    if (!ask.Deserialization(info))
    {
        message(ERROR, "服务器所接收到的是未遵守协议的字符串");
        std::string s = Response(0, -1).Serialization();
        int _send = Send(serversocket, s);
        if (_send == -1)
        {
            message(ERROR, "错误码：%d  服务器向客户端send失败:%s", errno, strerror(errno));
            return;
        }
        message(NORMAL, "客户端向服务器send成功");
    }
    else
    {
        // 反序列化成功,对数据进行计算处理，然后返回
        Response ret;
        switch (ask.op_)
        {
        case '+':
            ret.ret_ = ask.a_ + ask.b_;
            ret.state_code = 0;
            break;
        case '-':
            ret.ret_ = ask.a_ - ask.b_;
            ret.state_code = 0;
            break;
        case '*':
            ret.ret_ = ask.a_ * ask.b_;
            ret.state_code = 0;
            break;
        case '/':
            if (ask.b_ == 0)
            {
                ret.state_code = -1;
            }
            else
            {
                ret.ret_ = ask.a_ / ask.b_;
                ret.state_code = 0;
            }
            break;
        default:
            ret.state_code = -1;
            break;
        }
        std::string s(ret.Serialization());
        // std::cout << s << std::endl;
        int _send = Send(serversocket, s);
        if (_send == -1)
        {
            message(ERROR, "错误码：%d  服务器向客户端send结果失败:%s", errno, strerror(errno));
            return;
        }
        message(NORMAL, "客户端向服务器send结果成功");
    }
}
std::string Recv(int socket_, int *state) // 输出调用该接口的状况//0表示成功//1表示调用失败//2表示对方关闭，断开连接
{
    char buff[SIZE];
    std::string buffer;
    while (true)
    {
        ssize_t realsize = recv(socket_, buff, sizeof(buff) - 1, 0);
        if (realsize == -1)
        {
            *state = 1;
            return std::string("");
        }
        else if (realsize == 0)
        {
            *state = 2;
            return std::string("");
        }
        else
        {
            // 读取成功
            // 判断是否读取到了至少一个完整的报文
            buff[realsize] = 0;
            buffer += buff;
            int pos = buffer.find(SEP);
            if (pos == std::string::npos)
            {
                // 不是完整的报文
                continue;
            }
            // 走到这里，说明找到SEP了
            std::string length = buffer.substr(0, pos);
            message(NORMAL, "stoi前");
            message(NORMAL, "stoi:length:%s", length.c_str());
            int lenth = std::stoi(length);
            message(NORMAL, "stoi后");
            int expectsize = pos + SEPSIZE + lenth + SEPSIZE;
            if (expectsize > buffer.size())
            {
                // 不是完整的报文
                continue;
            }
            // 是完整的报文
            // 提取
            std::string package = buffer.substr(0, expectsize);
            buffer.erase(0, expectsize);
            // 对报文进行解码，解包，
            std::string parse_package = Decode(package);
            *state = 0;
            return parse_package;
        }
    }
}
int Send(int socket_, const std::string &s)
{

    std::string package = Encode(s);
    int realsend = send(socket_, package.c_str(), package.size(), 0);
    return realsend;
}
std::string Encode(const std::string &s)
{
    std::string package = std::to_string(s.size()); // 协议定制，使得接收方可以判断是否至少读取到一个完整的报文
    package += SEP;
    package += s;
    package += SEP;
    return package;
}
std::string Decode(const std::string &s)
{

    int lpos = s.find(SEP); // 对报文进行解码的前提是已经确认并提取到了完整的报文，所以如果调用这个接口，一定是保证了这个条件的，所以这里不用做判断
    std::string length = s.substr(0, lpos);
    // 提取报文
    std::string _message = s.substr(lpos + SEPSIZE, std::stoi(length));
    return _message;
}