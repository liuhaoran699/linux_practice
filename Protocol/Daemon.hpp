#pragma once
#include <signal.h>
#include <unistd.h>
#include <cerrno>
#include <cstdlib>
#include <cstring>
#include "Log.hpp"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
void create_daemon()
{
    // 对SIGPIPE和SIGCHLD设置为忽略,以此来防止这些信号对服务器的干扰
    signal(SIGPIPE, SIG_IGN);
    signal(SIGCHLD, SIG_IGN);
    // fork创建子进程，让子进程来充当守护进程，这样就保证了调用setsid的进程不是进程组的leader，
    pid_t pid = fork();
    if (pid < 0)
    {
        message(FATAL, "在创建守护进程时fork失败: 错误码[%d]%s", errno, strerror(errno));
        exit(-2);
    }
    else if (pid > 0)
        exit(0);
    // 走到这的是子进程
    // 调用setsid为该子进程创建对话，也就是这个子进程自成一个对话，成为了守护进程
    int sid = setsid();
    if (sid == -1)
    {
        message(FATAL, "构建守护进程时sid创建会话失败: 错误码[%d]%s", errno, strerror(errno));
        exit(-3);
    }
    message(NORMAL, "构建守护进程时sid会话创建成功:会话ID:%d", sid);

     // 对标准输入，输出，错误 进行重定向到 /dev/null这个文件中，不让其向终端中进行打印操作
    int _open=open("/dev/null",O_RDONLY|O_WRONLY);
    if(_open==-1)
    {
         message(FATAL, "构建守护进程时代开/dav/null文件失败: 错误码[%d]%s", errno, strerror(errno));
         exit(-4);
    }
    if(dup2(_open,0)==-1||dup2(_open,1)==-1||dup2(_open,2)==-1)
    {
         message(FATAL, "构建守护进程时重定向标准输入输出，错误失败: 错误码[%d]%s", errno, strerror(errno));
         exit(-5);
    }
     message(NORMAL, "构建守护进程时重定向成功");
    close(_open);
   
}
