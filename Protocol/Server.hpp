#pragma once
#include "Socket.hpp"
#include "Protocol.hpp"
#include <pthread.h>
#include <string>
#include<iostream>
#include <unordered_map>
#include <functional>
class Server
{
public:
    using _func = std::function<void(int, std::string)>;
    Server(const std::string &port) : port_(port)
    {
        socket_.Bind(port_);
        socket_.Listen();
    }
    static void *routine(void *args)
    {
        pthread_detach(pthread_self());
        ThreadData *data = (ThreadData *)args;
        int recvstate;
        std::string info = Recv(data->serversocket,&recvstate);
        if (recvstate == 1) // 接受信息失败
        {
            message(ERROR,"错误码：%d  服务器获取客户端消息recv失败:%s",errno,strerror(errno));
             close(data->serversocket);
              delete data;
            return nullptr;                                                                                    
        }
        else if (recvstate == 2) // 对方，关闭，断开连接
        {
            message(ERROR,"服务器尝试从客户端获取请求，但对方断开连接");
             close(data->serversocket);
              delete data;
            return nullptr;
        }
        else // 读取到客户端信息
        {
             message(NORMAL,"服务器从客户端获取信息recv成功");
             std::cout<<info<<std::endl;
            (data->server_->task)["计算"](data->serversocket, info);
        }
        close(data->serversocket);
        delete data; // 释放存储的客户端的相关数据
        return nullptr;
    }
    class ThreadData
    {
    public:
        ThreadData(Server *server, int sock, const std::string &ip, uint16_t port) : server_(server), serversocket(sock), ip_(ip), port_(port)
        {
        }

    public:
        int serversocket;
        std::string ip_;
        uint16_t port_;
        Server *server_;
    };
    void Addtask(const std::pair<std::string, _func> newtask)
    {
        task.insert(newtask);
    }
    void Start()
    {
        while (true)
        {
            std::string ip;
            uint16_t port;
            std::cout<<"开始等待接受请求"<<std::endl;
            int ServeId = socket_.Accept(&ip, &port);
            if (ServeId == -1)
            {
                // acce失败
                continue;
            }
            pthread_t tid;
            ThreadData *th = new ThreadData(this, ServeId, ip, port);
            pthread_create(&tid, nullptr, routine, th);
        }
    }

private:
    std::string port_;
    Socket socket_;
    std::unordered_map<std::string, _func> task;
};