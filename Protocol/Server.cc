#include"Server.hpp"
#include"Daemon.hpp"
#include"Protocol.hpp"
#include<string>
#include<iostream>
void Usage()
{
    std::cout<<"\nPlease use like this:  ./server ServerPort\n"<<std::endl;

}

int main(int argc,const char*argv[])
{
    if(argc!=2)
    {
        Usage();
        exit(11);
    }
    Server serve(argv[1]);
    serve.Addtask(std::make_pair(std::string("计算"),calculate));
    create_daemon();
    serve.Start();
    return 0;
}