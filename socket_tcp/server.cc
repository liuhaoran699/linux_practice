#include"ThreadPool/Log.hpp"
#include"server.hpp"
#include<memory>
void Usage()
{
    cout<<"\nPlease use like this: ./server port\n"<<endl;
}
int main(int argc,const char*argv[])
{
    if(argc!=2)
    {
        Usage();
        exit(7);
    }
    std::unique_ptr<Server>server(new Server((uint16_t)atoi(argv[1])));
    server->ServerInitial();
    server->Start();

    return 0;
}