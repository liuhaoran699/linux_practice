#pragma once
#include<string>
#include<functional>
class Task
{
using func=std::function<void(int)>;
public:
Task()
{}
Task(int serve_socket,func function):_serve_socket(serve_socket),_function(function)
{}
void operator()()
{
   _function(_serve_socket);
}
private:
int _serve_socket;
func _function;
};