#pragma once
#include"Lock.hpp"
class Cond
{
public:
Cond()
{
    pthread_cond_init(&cond,nullptr);
}
void Wait(Mutex* mutex)
{
    pthread_cond_wait(&cond,mutex->getmutex_addr());
}
void Signal()
{
    pthread_cond_signal(&cond);
}
~Cond()
{
    pthread_cond_destroy(&cond);
}
private:
pthread_cond_t cond;
};