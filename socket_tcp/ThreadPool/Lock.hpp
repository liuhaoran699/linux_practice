#pragma once
#include<pthread.h>
class Mutex
{
public:
Mutex()
{
    pthread_mutex_init(&mutex,nullptr);
}
void Lock()
{
    pthread_mutex_lock(&mutex);
}
void Unlock()
{
    pthread_mutex_unlock(&mutex);
}
~Mutex()
{
    pthread_mutex_destroy(&mutex);
}
pthread_mutex_t* getmutex_addr()
{
    return &mutex;
}
private:
pthread_mutex_t mutex;
};
class GuardLock
{
public:
GuardLock(Mutex*mutex):_mutex(mutex)
{
    _mutex->Lock();
}
~GuardLock()
{
    _mutex->Unlock();
}
private:
Mutex*_mutex;
};