#pragma once
#include<pthread.h>
#include<string>
class ThreadData
{
public:
ThreadData(const std::string&name,void*pool):_name(name),_pool(pool)
{}
int detach()
{
    return pthread_detach(pthread_self());
}
std::string _name;
void* _pool;
};
class Thread
{
public:
Thread(const std::string& name,void*pool):data(name,pool)
{}
void start(void*(*routine)(void*))
{
    pthread_create(&thread,nullptr,routine,(void*)&data);
}
~Thread()
{
    //pthread_join(thread,nullptr);
}
private:
pthread_t thread;
ThreadData data;
};