#include "Log.hpp"
#include <cstdio>
#include <string>
#include <cstring>
#include<unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <iostream>
#define SIZE 1024
void Usage()
{
    std::cout << "\nPlease use like this:  ./client ServerIP ServerPort\n " << std::endl;
}
int main(int argc, const char *argv[])
{

    if (argc != 3)
    {
        Usage();
        exit(-1);
    }
       
    while (1)//这里采用短连接的方式，避免单个客户端没做事但却一直占用服务器服务资源的情况(也就是避免长连接的情况，而选择短连接的情况)
    {

        std::string s;
        std::getline(std::cin, s);
        if (s == "quit")
        {
            break;
        }
        // 1.创建套接字为网络通信做准备
        int _socket = socket(AF_INET, SOCK_STREAM, 0);
        if (_socket == -1)
        {
            message(FATAL, "错误码：%d socket创建失败:%s", errno, strerror(errno));
            exit(1);
        }
        message(NORMAL, "socket创建成功");
    // 是否需要绑定套接字和端口号以及IP地址？
    // 客户端不需要bind端口号，因为一旦我们绑定，那一定是一个具体的端口号，但是我们并不清楚有没有其他的进程占用了这个端口号，
    // 所以我们设置的这个端口号 可能会与其他进程对应的端口号产生冲突，如果其他进程已经占用了该端口号，那么就会导致我们这个客户端无法启动的问题
    // 服务器一定要有一个明确的端口号，因为服务器面对的是众多的客户端，一旦服务器端口号出现更改，那么客户端就无法连接服务器了，所以服务器的端口号一经取用，不再改变
    // 客户端也一定要有一个端口号，但是我们对于客户端端口号的要求仅仅是要求它在对应主机的唯一性,对于它是多少，我们并没有要求
    // 所以客户端不需要我们显式地去bind端口号，但是一定是需要绑定端口号的，那么我们就需要让OS自动进行选择端口号进行绑定

        sockaddr_in server;
        memset(&server, 0, sizeof server);
        server.sin_family = AF_INET;
        int inet = inet_pton(AF_INET, argv[1], &server.sin_addr.s_addr);
        if (inet != 1)
        {
            message(FATAL, "错误码：%d IP地址转换不成功:%s", errno, strerror(errno));
            exit(2);
        }
        server.sin_port = htons(atoi(argv[2]));

    // 2.调用connect进行连接服务器
    // 客户端不需要进行设置套接字为监听状态(listen)，也不需要进行accept，因为客户端不需要有别人来连接它自己
    // 而是需要他自己有能够连接别人(服务端)的能力，而对应的接口就是connect
    // 如果connect调用成功，那么就意味着传过去的套接字文件描述符参数连接成功了
    // 并且调用connect以后，由于该接口是一个系统调用接口，所以他会自动帮我们把传过去的套接字绑定bind端口号，和IP（也就是它会帮我们绑定客户端的端口和IP）,
    // 使得后续我们能够使用该socket套接字进行通信
        int _connect = connect(_socket, (sockaddr *)&server, sizeof server);
        if (_connect == -1)
        {
            message(ERROR, "错误码：%d connect连接服务器失败:%s", errno, strerror(errno));
            continue;
        }
        message(NORMAL, "connect连接服务器成功");

        // 这里下面我们想要使用read和write接口和服务器之间进行通信也是没有问题的

        // 使用send向服务器发送消息数据
        // send是基于TCP来向目标服务器发送(写)消息的接口
        // send的前三个参数的含义和write的三个参数含义是一模一样的是一模一样的，返回值含义也相同//只多出了最后一个flags参数，这个参数我们一般设置为0就行了

        ssize_t _send = send(_socket, s.c_str(), s.size(), 0);
        if (_send == -1)
        {
            message(ERROR, "错误码：%d 向服务器发送请求失败:%s", errno, strerror(errno));
            continue;
        }
        message(NORMAL, "向服务器发送请求成功");

        // 使用recv接收服务器发来的数据结果
        // 对于读取服务器发送过来的信息，我们可以使用rec接口，同样的返回值以及前三个参数的含义和read接口是一样的//只多出了最后一个flags参数，这个参数我们一般设置为0就行了
        char buff[SIZE];
        ssize_t _recv = recv(_socket, buff, sizeof(buff) - 1, 0);
        if (_recv == -1)
        {
            message(ERROR, "错误码：%d 接收服务器处理结果失败:%s", errno, strerror(errno));
            continue;
        }
        message(NORMAL, "接收服务器处理结果成功");
        buff[_recv] = 0;
        std::cout << buff << std::endl;
        close(_socket);//每次处理完结果以后断开客户端与连接服务器的连接，
    }
    return 0;
}