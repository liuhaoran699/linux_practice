#include"ThreadPool/Log.hpp"
#include<cctype>
#include <string>
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <signal.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include"./ThreadPool/Task.hpp"
#include"./ThreadPool/ThreadPool.hpp"
#include<unordered_map>
using std::cout;
using std::endl;
#define LENGTH 10
#define SIZE 1024
std::unordered_map<std::string,std::string>WordMapping{{"apple","苹果"},{"hard","硬的"},{"certain","某种"}};
class Server
{
public:
    Server(uint16_t port, std::string addr = "") : internet_addr(addr), server_port(port), listen_socket(-1)
    {
    }
    void ServerInitial()
    {
        // 创建套接字
        listen_socket = socket(AF_INET, SOCK_STREAM, 0);
        if (listen_socket == -1)
        {
            message(FATAL, "错误码：%d socket创建失败:%s", errno, strerror(errno));
            exit(1);
        }
        message(NORMAL, "socket创建成功");
        // 设置套接字地址
        sockaddr_in server;
        memset(&server, 0, sizeof server);
        server.sin_family = AF_INET;
        server.sin_addr.s_addr = INADDR_ANY;
        server.sin_port = htons(server_port);
        // 使用bind绑定套接字与套接字地址
        int _bind = bind(listen_socket, (sockaddr *)&server, sizeof server);
        if (_bind == -1)
        {
            message(FATAL, "错误码：%d bind绑定失败:%s", errno, strerror(errno));
            exit(2);
        }
        message(NORMAL, "bind绑定成功");
        // 设置套接字为监听状态，为之后的连接做准备
        int _listen = listen(listen_socket, LENGTH);
        if (_listen == -1)
        {
            message(FATAL, "错误码：%d 套接字监听状态设置失败:%s", errno, strerror(errno));
            exit(3);
        }
        message(NORMAL, "套接字监听状态设置成功");
    }
   static void Serve(int _accept)
    {
        while (true)
        {
            char buff[SIZE];
            int _read = read(_accept, buff, sizeof(buff) - 1);// 如果客户端关闭了与服务器建立连接的套接字文件描述符，那么这里的返回值即为0
                                                              // 如果客户端与服务器建立了连接但是没有发送数据信息给服务器，那么该进程将阻塞在read这里
            if (_read == -1)
            {
                message(ERROR, "错误码：%d 读取客户端任务失败:%s", errno, strerror(errno));
                continue;
            }
            else if (_read == 0)
            {
                // 客户端断开连接
                cout << "client quit,me quit too!" << endl;
                break;
            }
            else
            {
                buff[_read] = 0;
                cout << buff << endl;
                // 将处理结果返回客户端
                //int _write = write(_accept, buff, strlen(buff));//对客户端实施回显服务
                
                // for(int i=0;i<_read;i++)
                // {
                //     if(islower(buff[i]))buff[i]=toupper(buff[i]);                   
                // }
                // int _write = write(_accept, buff, strlen(buff));//对客户端实施小写字母转大写字母服务
                
                 int _write;
                if(WordMapping.find(buff)!=WordMapping.end())//服务端实现客户端简单的英译汉请求
                {
                    const char* s =WordMapping[buff].c_str();
                    _write = write(_accept, s, strlen(s));
                }
                else{
                    _write = write(_accept, "我不知道", strlen("我不知道"));
                }
                if (_write == -1)
                {
                    message(ERROR, "错误码：%d 信息返回客户端失败:%s", errno, strerror(errno));
                    continue;
                }
            }
        }
        close(_accept);
    }
    /* void Start()
    {
        // 通过信号处理子进程的回收
        sighandler_t _sig = signal(SIGCHLD, SIG_IGN); // 显式地进行对子进程发送过来的SIGCHLD信号进行忽略，OS会帮我们进行子进程的回收，不用进行等待，避免子进程变为僵尸进程
        if (_sig == SIG_ERR)
        {
            message(FATAL, "错误码：%d 对SIGCHLD信号设置失败:%s", errno, strerror(errno));
            exit(4);
        }
        while (true)
        {
            sockaddr_in client;
            memset(&client, 0, sizeof client);
            socklen_t clenth = sizeof client;
            // 调用accept接收连接
            int _accept = accept(listen_socket, (sockaddr *)&client, &clenth); // 如果没有客户端连接，那么阻塞等待在这里// 成功连接，则返回套接字文件描述符(与监听套接字不同，这是用来进行通信服务的套接字),连接失败返回-1
            if (_accept == -1)
            {
                message(ERROR, "错误码：%d 套接字接收连接失败:%s", errno, strerror(errno));
                continue;
            }
            message(NORMAL, "套接字接收连接成功");
            // 获取连接
            //  处理客户端任务请求
            // 如果直接处理任务，那么如果当前客户持续连接服务器，那后续想要连接的客户端便无法马上连接
            // 解决方法：1.派生子进程去处理任务(子进程能够拿到父进程有的文件描述符，所以能够这么做),父进程只去做连接客户端的工作
            // 派生子进程处理任务这里给出两种方式

            // 创建进程的成本是比较高的，所以还可以有多线程的版本

            // 还可以有采用线程池的版本
            // 相比于前面的方式，线程池是有上限的，也就是说只能够同时处理线程池中线程数量的任务,那么这是否意味着线程池方案是不可行的，或者说比前面的方案更差的?
            // 并不是这样的，系统当中存在的线程数量并不是越多越好，线程池在一定程度上保护了服务器(因为它使得服务器的线程在一定数量之内)
            // 比如说当客户端持续发起大量请求，那么如果不对线程数量加以限制，那么服务端就会创建大量线程来处理这些请求，服务器很容易就挂掉了
            // 所以对于多进程和多线程的服务器，一定要标定一个线程和进程数量的明显的上限//否则就有可能由于在短时间内客户端传来大量的请求，导致服务器压力过大，进而导致OS杀掉我们的任务或者OS出现问题

            // 派生子进程处理任务----version1.0
            // 通过显式给出对子进程发送给父进程的SIGCHLD信号进行忽略处理来使得主进程不用等待即可让操作系统帮助我们回收子进程使子进程不会僵尸
            pid_t pid = fork();
            if (pid == -1)
            {
                message(ERROR, "错误码：%d 创建子进程失败:%s", errno, strerror(errno));
                continue;
            }
            else if (pid == 0)
            {
                // 子进程
                // 子进程只是为了处理客户端的任务，不需要监听套接字对应的文件描述符,所以尽量把它给关掉
                close(listen_socket);
                Serve(_accept);//传递用来进行对客户端进行服务的套接字
                exit(0);//处理任务结束，子进程退出
            }
            // 能够执行到这里，说明它是父进程
            // 父进程可以直接把用来接收客户端服务的套接字对应的文件描述符给关掉(因为父进程并不使用它)，反正也不影响子进程处理任务(因为文件的引用计数),
            // 并且如果不关掉，当有很多客户端进行连接，势必造成父进程可用的文件描述符越来越少，导致文件描述符不够用。所以这里把它给关掉，避免文件描述符泄漏。
            // 文件描述符数组在不同的linux内核版本中可能会不一样，有的是32，有的是64，这是标准上的一个进程能够打开的文件。但是作为云服务器来讲，一般一个进程所能够打开的文件描述符上限数量能够达到5万左右或者是10万
            close(_accept);
        }
    } */
    //  void Start()
    // {
    //     // 通过信号处理子进程的回收
    //     /* sighandler_t _sig = signal(SIGCHLD, SIG_IGN); // 显式地进行对子进程发送过来的SIGCHLD信号进行忽略，OS会帮我们进行子进程的回收，不用进行等待，避免子进程变为僵尸进程
    //     if (_sig == SIG_ERR)
    //     {
    //         message(FATAL, "错误码：%d 对SIGCHLD信号设置失败:%s", errno, strerror(errno));
    //         exit(4);
    //     } */
    //     while (true)
    //     {
    //         sockaddr_in client;
    //         memset(&client, 0, sizeof client);
    //         socklen_t clenth = sizeof client;
    //         // 调用accept接收连接
    //         int _accept = accept(listen_socket, (sockaddr *)&client, &clenth); // 如果没有客户端连接，那么阻塞等待在这里// 成功连接，则返回套接字文件描述符(与监听套接字不同，这是用来进行通信服务的套接字),连接失败返回-1
    //         if (_accept == -1)
    //         {
    //             message(ERROR, "错误码：%d 套接字接收连接失败:%s", errno, strerror(errno));
    //             continue;
    //         }
    //         message(NORMAL, "套接字接收连接成功");
    //         // 获取连接
    //         //  处理客户端任务请求
    //         // 如果直接处理任务，那么如果当前客户持续连接服务器，那后续想要连接的客户端便无法马上连接
    //         // 解决方法：1.派生子进程去处理任务(子进程能够拿到父进程有的文件描述符，所以能够这么做),父进程只去做连接客户端的工作
    //         // 派生子进程处理任务这里给出两种方式

    //         // 创建进程的成本是比较高的，所以还可以有多线程的版本

    //         // 还可以有采用线程池的版本
    //         // 相比于前面的方式，线程池是有上限的，也就是说只能够同时处理线程池中线程数量的任务,那么这是否意味着线程池方案是不可行的，或者说比前面的方案更差的?
    //         // 并不是这样的，系统当中存在的线程数量并不是越多越好，线程池在一定程度上保护了服务器(因为它使得服务器的线程在一定数量之内)
    //         // 比如说当客户端持续发起大量请求，那么如果不对线程数量加以限制，那么服务端就会创建大量线程来处理这些请求，服务器很容易就挂掉了
    //         // 所以对于多进程和多线程的服务器，一定要标定一个线程和进程数量的明显的上限//否则就有可能由于在短时间内客户端传来大量的请求，导致服务器压力过大，进而导致OS杀掉我们的任务或者OS出现问题

    //         // 派生子进程处理任务----version1.1
    //         // 派生子进程以后，又让子进程fork创建孙子进程，让孙子进程进行对客户端的服务，而后子进程立马退出，
    //         // 父进程阻塞等待子进程(由于子进程fork以后立马退出，所以对父进程来说并没有影响其效率)，
    //         // 而由于子进程立马退出，孙子进程一定会变成孤儿进程，然后被PID为1号的init进程领养，然后孙子进程接收，就由该进程为该孙子进程收尸
    //         pid_t pid = fork();
    //         if (pid == -1)
    //         {
    //             message(ERROR, "错误码：%d 创建子进程失败:%s", errno, strerror(errno));
    //             continue;
    //         }
    //         else if (pid == 0)
    //         {
    //             pid_t pid = fork();
    //             if (pid > 0)
    //                 exit(0);//子进程创建孙子进程以后立即结束该进程，使得父进程即使是调用waitpid接口阻塞式等待也不会对效率有所影响
    //             else if (pid == -1)
    //             {
    //                 message(ERROR, "错误码：%d 创建孙子进程失败:%s", errno, strerror(errno));
    //                 exit(5);
    //             }
    //             else//将对客户端的任务交给孙子进程来处理，由于子进程已经创建就退出，并且没有对孙子进程进行等待操作，
    //             {   //所以最后孙子进程一定会变为孤儿进程，从而被PID为1号的init进程所领养，并被其2进行收尸处理
    //                 // 孙子进程
    //                 // 孙子进程只是为了处理客户端的任务，不需要监听套接字对应的文件描述符,所以尽量把它给关掉
    //                 close(listen_socket);
    //                 Serve(_accept);
    //                 exit(0);//为客户端提供服务结束，孙子进程退出
    //             }
    //         }
    //         // 能够执行到这里，说明它是父进程
    //         // 父进程可以直接把用来接收客户端服务的套接字对应的文件描述符给关掉(因为父进程并不使用它)，反正也不影响子进程处理任务(因为文件的引用计数),
    //         // 并且如果不关掉，当有很多客户端进行连接，势必造成父进程可用的文件描述符越来越少，导致文件描述符不够用。所以这里把它给关掉，避免文件描述符泄漏。
    //         // 文件描述符数组在不同的linux内核版本中可能会不一样，有的是32，有的是64，这是标准上的一个进程能够打开的文件。但是作为云服务器来讲，一般一个进程所能够打开的文件描述符上限数量能够达到5万左右或者是10万
    //         close(_accept);
    //     }
    // }

     void Start()
    {
        // 通过信号处理子进程的回收
        /* sighandler_t _sig = signal(SIGCHLD, SIG_IGN); // 显式地进行对子进程发送过来的SIGCHLD信号进行忽略，OS会帮我们进行子进程的回收，不用进行等待，避免子进程变为僵尸进程
        if (_sig == SIG_ERR)
        {
            message(FATAL, "错误码：%d 对SIGCHLD信号设置失败:%s", errno, strerror(errno));
            exit(4);
        } */
         ThreadPool<Task>::GetThreadPool()->run();
        while (true)
        {
            sockaddr_in client;
            memset(&client, 0, sizeof client);
            socklen_t clenth = sizeof client;
            // 调用accept接收连接
            int _accept = accept(listen_socket, (sockaddr *)&client, &clenth); // 如果没有客户端连接，那么阻塞等待在这里// 成功连接，则返回套接字文件描述符(与监听套接字不同，这是用来进行通信服务的套接字),连接失败返回-1
            if (_accept == -1)
            {
                message(ERROR, "错误码：%d 套接字接收连接失败:%s", errno, strerror(errno));
                continue;
            }
            message(NORMAL, "套接字接收连接成功");
            // 获取连接
            //  处理客户端任务请求
            // 如果直接处理任务，那么如果当前客户持续连接服务器，那后续想要连接的客户端便无法马上连接
            // 解决方法：1.派生子进程去处理任务(子进程能够拿到父进程有的文件描述符，所以能够这么做),父进程只去做连接客户端的工作
            // 派生子进程处理任务这里给出两种方式

            // 创建进程的成本是比较高的，所以还可以有多线程的版本

            // 还可以有采用线程池的版本
            // 相比于前面的方式，线程池是有上限的，也就是说只能够同时处理线程池中线程数量的任务,那么这是否意味着线程池方案是不可行的，或者说比前面的方案更差的?
            // 并不是这样的，系统当中存在的线程数量并不是越多越好，线程池在一定程度上保护了服务器(因为它使得服务器的线程在一定数量之内)
            // 比如说当客户端持续发起大量请求，那么如果不对线程数量加以限制，那么服务端就会创建大量线程来处理这些请求，服务器很容易就挂掉了
            // 所以对于多进程和多线程的服务器，一定要标定一个线程和进程数量的明显的上限//否则就有可能由于在短时间内客户端传来大量的请求，导致服务器压力过大，进而导致OS杀掉我们的任务或者OS出现问题
            //线程池方案
            Task task(_accept,Serve);
            ThreadPool<Task>::GetThreadPool()->push(task);
            // 派生子进程处理任务----version1.1
            // 派生子进程以后，又让子进程fork创建孙子进程，让孙子进程进行对客户端的服务，而后子进程立马退出，
            // 父进程阻塞等待子进程(由于子进程fork以后立马退出，所以对父进程来说并没有影响其效率)，
            // 而由于子进程立马退出，孙子进程一定会变成孤儿进程，然后被PID为1号的init进程领养，然后孙子进程接收，就由该进程为该孙子进程收尸
           /*  pid_t pid = fork();
            if (pid == -1)
            {
                message(ERROR, "错误码：%d 创建子进程失败:%s", errno, strerror(errno));
                continue;
            }
            else if (pid == 0)
            {
                pid_t pid = fork();
                if (pid > 0)
                    exit(0);//子进程创建孙子进程以后立即结束该进程，使得父进程即使是调用waitpid接口阻塞式等待也不会对效率有所影响
                else if (pid == -1)
                {
                    message(ERROR, "错误码：%d 创建孙子进程失败:%s", errno, strerror(errno));
                    exit(5);
                }
                else//将对客户端的任务交给孙子进程来处理，由于子进程已经创建就退出，并且没有对孙子进程进行等待操作，
                {   //所以最后孙子进程一定会变为孤儿进程，从而被PID为1号的init进程所领养，并被其2进行收尸处理
                    // 孙子进程
                    // 孙子进程只是为了处理客户端的任务，不需要监听套接字对应的文件描述符,所以尽量把它给关掉
                    close(listen_socket);
                    Serve(_accept);
                    exit(0);//为客户端提供服务结束，孙子进程退出
                }
            }
            // 能够执行到这里，说明它是父进程
            // 父进程可以直接把用来接收客户端服务的套接字对应的文件描述符给关掉(因为父进程并不使用它)，反正也不影响子进程处理任务(因为文件的引用计数),
            // 并且如果不关掉，当有很多客户端进行连接，势必造成父进程可用的文件描述符越来越少，导致文件描述符不够用。所以这里把它给关掉，避免文件描述符泄漏。
            // 文件描述符数组在不同的linux内核版本中可能会不一样，有的是32，有的是64，这是标准上的一个进程能够打开的文件。但是作为云服务器来讲，一般一个进程所能够打开的文件描述符上限数量能够达到5万左右或者是10万
            close(_accept); */
        }
    }
    ~Server()
    {}

private:
    std::string internet_addr;
    uint16_t server_port;
    int listen_socket;
};