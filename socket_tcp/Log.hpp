#pragma once
#include<cstdio>
#include<stdarg.h>
#include<ctime>
#define DEBUG 0
#define NORMAL 1
#define WARNING 2
#define ERROR 3
#define FATAL 4
const char* level_map[5]={"DEBUG","NORMAL","WARNING","ERROR","FATAL"};
void message(int level,const char*fomat,...)
{
#ifndef _DEBUG
    if(level==DEBUG)return;
#endif
    char stdbuffer[1024];
    time_t timestamp=time(nullptr);
    struct tm*local=localtime(&timestamp);
    snprintf(stdbuffer,sizeof stdbuffer,"日志等级：%s  时间：%d年%d月%d日%d时%d分%d秒\n",level_map[level],local->tm_year+1900,local->tm_mon+1,local->tm_mday,local->tm_hour,local->tm_min,local->tm_sec);
    char logbuffer[1024];
    va_list argv;
    va_start(argv,fomat);
    vsnprintf(logbuffer,sizeof logbuffer,fomat,argv);
    va_end(argv);
    FILE*file=fopen("log.txt","a");
    fprintf(file,"标准日志\n%s自定义日志\n%s\n",stdbuffer,logbuffer);
    fclose(file);
}