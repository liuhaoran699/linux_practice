#include"udp_server_talk.hpp"
#include<iostream>
using std::cout;
using std::endl;
void Usage()
{
    cout<<"Please use it like :\n";
    cout<<"\n   ./client "<<"IP address "<<"destination port\n"<<endl;
}
int main(int argc,const char*argv[])
{
    if(argc!=3)
    {
        Usage();
        exit(4);
    }
   Server serve(argv[1],(uint16_t)atoi(argv[2]));
   serve.server_initial();
   serve.servertoclient();
    return 0;
}
