#pragma once
#include<pthread.h>
#include<string>
class ThreadData
{
public:
ThreadData(const std::string&name,void*arg):_name(name),_arg(arg)
{}
std::string _name;
void* _arg;
};


class Thread
{
public:
Thread(const std::string& name,void*arg):data(name,arg)
{}
void start(void*(*routine)(void*))
{
    pthread_create(&thread,nullptr,routine,(void*)&data);
}
~Thread()
{
    pthread_join(thread,nullptr);
}
private:
pthread_t thread;
ThreadData data;
};