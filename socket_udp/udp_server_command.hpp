#include "Log.hpp"
#include <errno.h>
#include <iostream>
#include <sys/types.h>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string>
#include <cstring>
#include <cstring>
#define SIZE 1024
class Server
{
public:
    Server(const std::string &addr, uint16_t port) : internet_addr(addr), _port(port)
    {
    }
    void server_initial()
    {
        // 创建套接字
         _socket = socket(AF_INET, SOCK_DGRAM, 0);//成功，则返回一个对应该套接字的文件描述符
         std::cout<<_socket<<std::endl;
        if (_socket == -1)//socket调用失败返回-1
        {
            message(FATAL, "错误码%d,socket:%s", errno, strerror(errno));
            exit(1);
        }
        // 创建套接字成功
        // 套接字地址结构的设置
        sockaddr_in local;
        bzero(&local, sizeof local); // 对local进行清0，使得之后local设置完成以后，后面的填充部分为0
        // 套接字创建的第一个参数是什么,这里就填什么，这里和那里一样
        local.sin_family = AF_INET;
        // IP地址的设置
        // 我们拿到的是点分十进制的string，点把字符串分成4部分，每部分的数值在0~255之间，也就是1字节，所以相比于使用字符串，可以用4个字节来表示，在网络传输中更有利于传输效率。
        // 将其转换成4字节后，还要按照网络字节序(大端)存储在其中
        //inet_aton能够帮助我们把点分十进制的字符串转换成4字节形式来表示并且按照网络字节序存储在一个我们提供的struct in_addr的结构体中(对应的第二个参数in_addr*,输出型参数)
        if (inet_aton(internet_addr.c_str(), &local.sin_addr) == 0) // 该函数不会设置errno//如果给的是无效的IP，那么返回0，否则为非0.
        {
            message(FATAL, "inet_aton:%s", "无效的IP");
            exit(2);
        }
        // 端口号的设置
        // 同样的要按照网络字节序(大端)存储在其中
        local.sin_port = htons(_port);

        // 绑定通过套接字文件描述符绑定我们创建的套接字和套接字地址
        if (bind(_socket, (sockaddr *)&local, sizeof local) == -1)
        {
            message(FATAL, "错误码%d,bind:%s", errno, strerror(errno));
            exit(3);
            std::cout<<"exit?"<<std::endl;
        }
    }
    void servertoclient()
    {

        while (true)
        {
            char buff[SIZE];
            sockaddr_in client;//作为后面recvfrom的输出型参数来获取客户端的套接字地址
            bzero(&client, sizeof client);
            socklen_t addrlen = sizeof client;//作为后面recvfrom的输入输出型参数，向recvfrom接口输入client对象的大小,并通过该接口输出得到客户端的套接字地址的大小
            int r_size=recvfrom(_socket, buff, sizeof(buff)-1, 0, (sockaddr *)&client, &addrlen);
            if (r_size== -1)//recvfrom第三个参数表示最多接收到多少字节大小的数据,返回值表示实际接收到的字节数(失败返回-1)//第四个参数为0表示阻塞式地等待接收
            {
                message(ERROR, "错误码%d,recvfrom:%s", errno, strerror(errno));
                continue;
            }
            buff[r_size]=0;//字符串末尾置为'\0'
            // 接收到数据，对数据进行处理//模拟远程控制，通过客户端远程控制服务器的行为
            FILE*result=popen(buff,"r");//popen接口接受shell指令，并执行该指令，返回一个FILE*，如果是以读方式打开这个文件，那么读取到该指令的标准输出，如果是以写方式代开该文件，那么
            char ret[1024];             //我们可以对该文件写入内容，该内容作为执行指令的标准输入内容
            int truesize=fread(ret,1,sizeof(ret)-1,result);
            ret[truesize]=0;
            std::cout<<ret<<std::endl;
            pclose(result);
            //将处理后的数据发送回客户端
            int w_size=sendto(_socket, buff, r_size, 0, (sockaddr *)&client, addrlen);//最后两个参数对应的是客户端的套接字地址信息，通过上面的recvfrom我们已经获取到了
            if ( w_size== -1)//sendto第三个参数表示最多发送的数据大小，返回值表示实际发送的字节数(失败返回-1)//第四个参数为0表示阻塞式地发送
            {
                message(ERROR, "错误码%d,sendto:%s", errno, strerror(errno));
                continue;
            }
        }
    }
    ~Server()
    {
        close(_socket);
    }

private:
    std::string internet_addr;
    uint16_t _port;
    int _socket;
};