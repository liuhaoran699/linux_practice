#include <iostream>
#include"Log.hpp"
#include<sys/types.h>
#include<sys/socket.h>
#include<errno.h>
#include <string>
#include<string.h>
#include<arpa/inet.h>
#include<unistd.h>
using std::cout;
using std::endl;
void Usage()
{
    cout<<"Please use it like :\n";
    cout<<"\n   ./client "<<"IP address "<<"destination port\n"<<endl;
}
int main(int argc,const char*argv[])//通过命令行选项接接收服务端的IP和端口//对于云服务器来说我们不能够设置IP为我们主机的IP，并且也不推荐这么做，
{                                   //我们可以设置IP为127.0.0.1表示本地回环
                                    //也可以设置为0.0.0.0(或者在后续的时候对socket_in类对象的sin_addr成员结构中的s_addr设置为INADDR_ANY)
                                    //表示的是一个服务器上所有的网卡(服务器可能不止一个网卡,每个网卡上有不同的IP地址),
    if(argc!=3)                     //多个本地ip地址都进行绑定端口号,这样当客户端请求发送到服务器的这个端口，不管是哪个网卡/哪个IP地址接收到的数据，都是可以被改服务器所处理的
    {                               //如果绑定死了某一个IP，只有客户端向这个指定的IP发送数据，才会被该服务器处理。
        Usage();                    //所以我们更加推荐设置IP为0.0.0.0或者INADDR_ANY
        exit(1);                            
    }
    //创建套接字
        int _socket=socket(AF_INET,SOCK_DGRAM,0);
        if(_socket==-1)
        {
            message(FATAL,"错误码%d,socket:",errno,strerror(errno));
            exit(2);
        }
        //设置服务端套接字地址
        sockaddr_in server;
        bzero(&server,sizeof server);
        server.sin_family=AF_INET;
        if(!inet_aton(argv[1],&server.sin_addr))
        {
            message(FATAL, "inet_aton:%s", "无效的IP");
            exit(3);
        }
        server.sin_port=htons((uint16_t)atoi(argv[2]));
    while (true)
    {
        cout << "Please input your requests:";
        std::string input;
        getline(std::cin, input);
        if(input=="quit")//退出服务
        {
            close(_socket);
            return 0;
        }
        message(NORMAL,"输入请求完毕，即将发送");
        if(sendto(_socket,input.c_str(),input.size(),0,(sockaddr*)&server,sizeof server)==-1)
        {
            message(ERROR,"错误码%d,sendto:",errno,strerror(errno));
            continue;
        }
        message(NORMAL,"发送内容完毕");
        char buff[1024];
        sockaddr_in _server;//作为recvfrom的输出型参数来获取服务端的IP和端口
        socklen_t server_size=sizeof _server;//作为后面recvfrom的输入输出型参数，向recvfrom接口输入_server对象的大小,并通过该接口输出得到服务端的套接字地址的大小
        int result=recvfrom(_socket,buff,sizeof(buff)-1,0,(sockaddr*)&_server,&server_size);//获取服务端对我们传输过去的数据的处理结果
        if(result==-1)
        {
            message(ERROR,"错误码%d,recvfrom:",errno,strerror(errno));
            continue;
        }
         message(NORMAL,"接收服务端数据完毕");
        buff[result]=0;
        cout<<"echo:"<<buff<<endl;
    }

    return 0;
}