#define _CRT_SECURE_NO_WARNINGS 1
#define  _WINSOCK_DEPRECATED_NO_WARNINGS
#include<iostream>
#include<string>
#include<WinSock2.h>//在Window下使用套接字要包含此头文件
#pragma comment(lib,"ws2_32.lib")//第一个lib表示链接库，后面的字符串表示的是链接什么库(这里链接的是一个静态库)//进行套接字编程，在Windows下要链接ws2_32.lib（可以认为是固定写法）

int main()
{

	WORD sockversion = MAKEWORD(2, 2);//socket编程中：声明调用不同的Winsock版本。例如MAKEWORD(2,2)就是调用2.2版，MAKEWORD(1,1)就是调用1.1版。
									  //不同版本是有区别的，例如1.1版只支持TCP/IP协议，此外winsock 2.0支持异步 1.1不支持异步.
									  //而2.0版可以支持多协议。2.0版有良好的向后兼容性，
									  //任何使用1.1版的源代码、二进制文件、应用程序都可以不加修改地在2.0规范下使用。
	//windows下加载套接字库并进行版本协商
	WSADATA WSAData;
	LPWSADATA WSADataPtr=&WSAData;
	int ret = WSAStartup(sockversion, WSADataPtr);
		if (ret)
		{
			return ret;
		}



		SOCKET _socket = socket(AF_INET, SOCK_DGRAM, 0);
		char buff[1024];
		std::string serverID = "101.33.247.117";
		SOCKADDR_IN server;
		server.sin_family = AF_INET;
		server.sin_addr.S_un.S_addr = inet_addr(serverID.c_str());
		server.sin_port = htons(8080);
		
		int lenth=sizeof server;
		//客户端不需要我们进行绑定，当需要的时候，OS会帮我们绑定
		while (1)
		{
			std::string input;
			std::getline(std::cin, input);
			sendto(_socket, input.c_str(), input.size(), 0, (sockaddr*)&server, sizeof server);
			int real_size = recvfrom(_socket, buff, sizeof(buff) - 1, 0,(sockaddr*)&server,&lenth);
			if (SOCKET_ERROR == real_size)
			{
				return WSAGetLastError();
			}
			buff[real_size] = 0;
			std::cout << buff << std::endl;
			
		}


		closesocket(_socket);
		WSACleanup();
	
	return 0;
}