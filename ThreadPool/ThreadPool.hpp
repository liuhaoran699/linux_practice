#pragma once
#include <vector>
#include <string>
#include <queue>
#include "Thread.hpp"
#include "Lock.hpp"
#include "Cond.hpp"
int default_num = 4;
template <typename Task>
class ThreadPool
{
public:
    static ThreadPool *GetThreadPool()
    {
        if (singleton == nullptr)
        {
            GuardLock Lock(&mutex);
            if (singleton == nullptr)
            {
                singleton = new ThreadPool(5);
            }
        }
        return singleton;
    }
    bool isempty()
    {
        return task_queue.size() == 0;
    }
    static void *routine(void *args)
    {
        ThreadData *data = (ThreadData *)args;
        ThreadPool *pool = (ThreadPool *)data->_pool;
        while (1)
        {
            Task task;
            {
                GuardLock Lock(&mutex);
                while (pool->isempty())
                    pool->cond.Wait(&mutex);
                pool->pop(&task);
            }
            message(NORMAL, "[%s] %d+%d=%d\n", data->_name.c_str(), task.get_first(), task.get_second(), task());
        }
    }
    void run()
    {
        for (int i = 0; i < pool.size(); i++)
        {
            pool[i]->start(routine);
            message(NORMAL, "线程%d创建成功\n", i + 1);
        }
    }
    void push(const Task &task)
    {
        GuardLock Lock(&mutex);
        task_queue.push(task);
        cond.Signal();
    }
    // Mutex *getmutex_addr()
    // {
    //     return &mutex;
    // }
    void pop(Task *task)
    {
        *task = task_queue.front();
        task_queue.pop();
    }
    ~ThreadPool()
    {
        for (int i = 0; i < pool.size(); i++)
            delete pool[i];
    }

private:
    ThreadPool(int th_num = default_num) : thread_num(th_num), pool(th_num)
    {
        for (int i = 0; i < pool.size(); i++)
        {
            pool[i] = new Thread(std::string("线程") + std::to_string(i + 1), this);
        }
    }
    ThreadPool(const ThreadPool &x) = delete;

private:
    std::queue<Task> task_queue;
    std::vector<Thread *> pool;
    static Mutex mutex;
    Cond cond;
    int thread_num;
    static ThreadPool *singleton;
};
template <class Task>
ThreadPool<Task> *ThreadPool<Task>::singleton = nullptr;
template<class Task>
Mutex ThreadPool<Task>::mutex;