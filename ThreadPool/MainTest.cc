#include<iostream>
using std::cout;
using std::endl;
#include"Log.hpp"
#include<sys/types.h>
#include<unistd.h>
#include<pthread.h>
#include<cstdlib>
#include<ctime>
#include"ThreadPool.hpp"
#include"Task.hpp"
int add(int a,int b)
{
    return a+b;
}
int main()
{
    //ThreadPool<Task>pool;
    srand(time(nullptr)^getpid());
    //pool.run();
    ThreadPool<Task>::GetThreadPool()->run();
    sleep(2);
    while(1)
    {
        int a=rand()%10+1;
        int b=rand()%20+1;
        //pool.push(Task(a,b,add));
         ThreadPool<Task>::GetThreadPool()->push(Task(a,b,add));
        message(NORMAL,"派发任务%d+%d=?\n",a,b);
        sleep(1);
    }
    //message(1,"这是测试代码%d",1);
    return 0;
}