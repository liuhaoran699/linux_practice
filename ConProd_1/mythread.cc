#include<iostream>
#include<string>
#include<pthread.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/time.h>
#include<assert.h>
#include<cstdlib>
#include<ctime>
using std::cout;
using std::endl;
// void * threadroutine(void*routine)
// {
//     int count=0;
//     while(true)
//     {
//         std::cout<<(char*)routine<<" pid:"<<getpid()<<"  "<<count<<std::endl;
//         sleep(1);
//         // int num=10;//线程异常造成的影响
//         // num/=0;
//         if(++count==2)break;
//     }
//     //pthread_exit((void*)15);//线程退出的接口
//     //exit(3);//exit是退出进程的，在一个进程内，不管在哪调用，一旦调用进程退出
//     cout<<"新线程退出  "<<count<<endl;
//     return (void*)3;

// }
// int main()
// {
    
//     pthread_t thid;
//     pthread_create(&thid,nullptr,threadroutine,(void*)"新线程执行任务");
//     int count=0;
//     while(true)
//     {
//         std::cout<<"这是主线程"<<" pid:"<<getpid()<<std::endl;
//         sleep(1);
//         if(++count==5)break;
//     }
//     pthread_cancel(thid);
//     void*ret=nullptr;
//     pthread_join(thid,&ret);
//     cout<<"新线程退出返回值"<<(long long)ret<<endl;
//    /*  while(true)
//     {
//         std::cout<<"这是主线程"<<" pid:"<<getpid()<<std::endl;
//         sleep(1);
//     } */
//     return 0;
// }
//*******************************************************************
/* __thread int g_val=0;
void * threadroutine(void*routine)
{ 
    while(true)
    {
        std::cout<<(char*)routine<<" pid:"<<getpid()<<"  "<<"g_val:"<<g_val<<std::endl;
        g_val++;
        sleep(1);
        
    }   
    return (void*)1;
}
int main()
{
    pthread_t tid;
    assert(!pthread_create(&tid,nullptr,threadroutine,(void*)"this is new thread....  "));
    while(true)
    {
        std::cout<<"this is main thread....  "<<" pid:"<<getpid()<<"  "<<"g_val:"<<g_val<<std::endl;
        sleep(1);
    }
    return 0;
}
 */
/* #include<iostream>
#include<thread>
#include<pthread.h>
#include<unistd.h>
#include<sys/types.h>
#include<assert.h>
using std::cout;
using std::endl;
void fun()
{
    while(true)
    {
        cout << "hello new thread" << endl;
        sleep(1);
    }
}

int main()
{
    std::thread t(fun);
    std::thread t1(fun);
    std::thread t2(fun);
    std::thread t3(fun);
    std::thread t4(fun);

    while(true)
    {
        cout << "hello main thread" << endl;
        sleep(1);
    }

    t.join();
} */
//********************************************************
//不使用接口pthread_mutex_init进行初始化
/* pthread_mutex_t mutex;//=PTHREAD_MUTEX_INITIALIZER;
int tickets=10000;
void* buytickets(void*args)
{
    srand(time(nullptr));
    pthread_detach(pthread_self());
    while(true)
    {
        usleep((rand()*pthread_self()^0xFF754)%10000);
        pthread_mutex_lock(&mutex);
        if(tickets>0)
        {
        usleep((rand()*pthread_self()^0xFF754)%10000);//模拟抢票的时间
        cout<<pthread_self()<<" is buying a ticket now,the rest of tickets:"<<tickets<<endl;
        tickets--;
        pthread_mutex_unlock(&mutex);
        }
        else 
        {
        pthread_mutex_unlock(&mutex);
        break;
        }
        //pthread_mutex_unlock(&mutex); //不能在这里进行解锁操作，因为一旦线程执行break,那么也就执行不了解锁操作，因为我们定义的锁是
                                        //全局的，我们如果设置了锁，但是没有解锁，那么其他的线程也没法执行上锁的操作，也就没法执行pthread_mutex_lock(&mutex);以后的代码了
    }
}
int main()
{
    pthread_mutex_init(&mutex,nullptr);
    pthread_t th1,th2,th3,th4,th5;
    pthread_create(&th1,nullptr,buytickets,nullptr);
    pthread_create(&th2,nullptr,buytickets,nullptr);
    pthread_create(&th3,nullptr,buytickets,nullptr);
    pthread_create(&th4,nullptr,buytickets,nullptr);
    pthread_create(&th5,nullptr,buytickets,nullptr);

    while(1)sleep(1);
} */
//********************************************************
//使用pthread_mutex_init进行互斥锁的初始化，并且将互斥锁定义成局部变量进行操作
/* #define TH_NUM 5
pthread_t Thread[TH_NUM];
class ThreadData
{
public:
    ThreadData(std::string name,pthread_mutex_t*mutex_addr):name(name),mutex(mutex_addr)
    {}
    std::string name;
    pthread_mutex_t* mutex;
};
int tickets=1000;
void* buytickets(void*args)
{
    ThreadData*attr=(ThreadData*)args;
    srand(time(nullptr));
    while(true)
    {
        usleep((rand()*pthread_self()^0xFF754)%10000);
        assert(!pthread_mutex_lock(attr->mutex));
        if(tickets>0)
        {
        usleep((rand()*pthread_self()^0xFF754)%10000);//模拟抢票的时间
        cout<<attr->name<<" is buying a ticket now,the rest of tickets:"<<tickets<<endl;
        tickets--;
        assert(!pthread_mutex_unlock(attr->mutex));
        }
        else 
        {
        assert(!pthread_mutex_unlock(attr->mutex));
        break;
        }
        //pthread_mutex_unlock(&mutex); //不能在这里进行解锁操作，因为一旦线程执行break,那么也就执行不了解锁操作，因为我们定义的锁是
                                        //全局的，我们如果设置了锁，但是没有解锁，那么其他的线程也没法执行上锁的操作，也就没法执行pthread_mutex_lock(&mutex);以后的代码了
    }
    delete attr;
}
int main()
{
    struct timeval tvbg,tved;
    assert(!gettimeofday(&tvbg,nullptr));
    pthread_mutex_t mutex;
    assert(!pthread_mutex_init(&mutex,nullptr));

    for(int i=0;i<TH_NUM;i++)
    {
        ThreadData*th=new ThreadData(std::string("thread")+std::to_string(i+1),&mutex);
        assert(!pthread_create(Thread+i,nullptr,buytickets,(void*)th));
    }
    for(int i=0;i<TH_NUM;i++)
    {     
       assert(!pthread_join(Thread[i],nullptr));
    }
    assert(!pthread_mutex_destroy(&mutex));
    assert(!gettimeofday(&tved,nullptr));
    cout<<(tved.tv_sec-tvbg.tv_sec)<<"seconds" <<endl;
    cout<<(tved.tv_usec-tvbg.tv_usec)<<"microseconds" <<endl;
}  */
/* #define TH_NUM 5
pthread_t Thread[TH_NUM];
class ThreadData
{
public:
    ThreadData(std::string name,pthread_mutex_t*mutex_addr):name(name),mutex(mutex_addr)
    {}
    std::string name;
    pthread_mutex_t* mutex;
};
int tickets=1000;
void* buytickets(void*args)
{
    ThreadData*attr=(ThreadData*)args;
    srand(time(nullptr));
    while(true)
    {
        usleep((rand()*pthread_self()^0xFF754)%10000);
        assert(!pthread_mutex_lock(attr->mutex));
        if(tickets>0)
        {
        usleep((rand()*pthread_self()^0xFF754)%10000);//模拟抢票的时间
        cout<<attr->name<<" is buying a ticket now,the rest of tickets:"<<tickets<<endl;
        tickets--;
        assert(!pthread_mutex_lock(attr->mutex));
        }
        else 
        {
        assert(!pthread_mutex_lock(attr->mutex));
        //break;
        }
        //pthread_mutex_unlock(&mutex); //不能在这里进行解锁操作，因为一旦线程执行break,那么也就执行不了解锁操作，因为我们定义的锁是
                                        //全局的，我们如果设置了锁，但是没有解锁，那么其他的线程也没法执行上锁的操作，也就没法执行pthread_mutex_lock(&mutex);以后的代码了
    }
    delete attr;
}
int main()
{
    struct timeval tvbg,tved;
    assert(!gettimeofday(&tvbg,nullptr));
    pthread_mutex_t mutex;
    assert(!pthread_mutex_init(&mutex,nullptr));

    for(int i=0;i<TH_NUM;i++)
    {
        ThreadData*th=new ThreadData(std::string("thread")+std::to_string(i+1),&mutex);
        assert(!pthread_create(Thread+i,nullptr,buytickets,(void*)th));
    }
    for(int i=0;i<TH_NUM;i++)
    {     
       assert(!pthread_join(Thread[i],nullptr));
    }
    assert(!pthread_mutex_destroy(&mutex));
    assert(!gettimeofday(&tved,nullptr));
    cout<<(tved.tv_sec-tvbg.tv_sec)<<"seconds" <<endl;
    cout<<(tved.tv_usec-tvbg.tv_usec)<<"microseconds" <<endl;
} 
 */
/* typedef void (*func)(std::string&name,pthread_mutex_t*mutex,pthread_cond_t*cond);
#define TNUM 5
pthread_t Thread[TNUM];
class ThreadData
{
public:
    ThreadData(const std::string&name,pthread_mutex_t*mutex,pthread_cond_t*cond,func task)
    :_name(name),_mutex(mutex),_cond(cond),_task(task)
    {}
public:
    std::string _name;
    pthread_mutex_t* _mutex;
    pthread_cond_t* _cond;
    func _task;
};
volatile int quit=0;
void start_routine1(std::string&name,pthread_mutex_t*mutex,pthread_cond_t*cond)
{
    while(!quit)
    {   
        pthread_mutex_lock(mutex);//如果每一个线程都没有这个加锁与解锁操作，那么当因为pthread_cond_wait而陷入阻塞挂起的线程接收到通知被唤醒的时候，会将该mutex互斥锁归还给这个线程
        pthread_cond_wait(cond,mutex);//也就是说，第一个被唤醒的线程去申请这个mutex互斥锁时会申请成功并持有这个mutex互斥锁，但是由于这几个线程调用pthread_cond_wait都使用的是同一个mutex，那么随后被唤醒的线程
        cout<<name<<" is running"<<endl;//也都会去申请这个互斥锁，但是这个互斥锁被第一个唤醒的线程所占用，并且它也没有释放mutex锁的操作，所以这些线程会申请锁申请失败，也就是说将不会持有这个mutex互斥锁，
        pthread_mutex_unlock(mutex);    //因此，这些个线程会继续阻塞在pthread_cond_wait这个位置。
        sleep(20);
    }
    cout<<name<<"quit"<<endl;
}
void start_routine2(std::string&name,pthread_mutex_t*mutex,pthread_cond_t*cond)
{
    while(!quit)
    {   
        pthread_mutex_lock(mutex);
        pthread_cond_wait(cond,mutex);
        cout<<name<<" is running"<<endl;
        pthread_mutex_unlock(mutex);
        sleep(20);
    }
    cout<<name<<"quit"<<endl;
}
void start_routine3(std::string&name,pthread_mutex_t*mutex,pthread_cond_t*cond)
{
    while(!quit)
    {   
        pthread_mutex_lock(mutex);
        pthread_cond_wait(cond,mutex);
        cout<<name<<" is running"<<endl;
        pthread_mutex_unlock(mutex);
        sleep(20);
    }
    cout<<name<<"quit"<<endl;
}
void start_routine4(std::string&name,pthread_mutex_t*mutex,pthread_cond_t*cond)
{
    while(!quit)
    {
        pthread_mutex_lock(mutex);
        pthread_cond_wait(cond,mutex);
        cout<<name<<" is running"<<endl;
        pthread_mutex_unlock(mutex);
        sleep(20);
    }
    cout<<name<<"quit"<<endl;
}
void start_routine5(std::string&name,pthread_mutex_t*mutex,pthread_cond_t*cond)
{
    while(!quit)
    {
        pthread_mutex_lock(mutex);
        pthread_cond_wait(cond,mutex);
        cout<<name<<" is running"<<endl;
        pthread_mutex_unlock(mutex);
        sleep(20);
    }
    cout<<name<<"quit"<<endl;
}
void* Entry(void*args)
{
    ThreadData*Th=(ThreadData*)args;
    Th->_task(Th->_name,Th->_mutex,Th->_cond);
}
int main()
{
    pthread_cond_t cond;
    pthread_mutex_t mutex;
    func func_n[TNUM]={start_routine1,start_routine2,start_routine3,start_routine4,start_routine5};
    pthread_mutex_init(&mutex,nullptr);
    pthread_cond_init(&cond,nullptr);
    for(int i=0;i<TNUM;i++)
    {
        ThreadData*Th=new ThreadData(std::string("Thread")+std::to_string(i+1),&mutex,&cond,func_n[i]);
        pthread_create(Thread+i,nullptr,Entry,(void*)Th);
        sleep(1);
    }
    int count=TNUM;
    sleep(3);
    while(count--)
    {
       pthread_cond_signal(&cond);//按照某种顺序把线程依次唤醒，使得各个线程都能得到调度
       sleep(3);//给时间让线程把锁给释放了，让其他线程好拿到锁，然后才好执行后面的pthread_cond_wait来响应这个pthread_cond_signal 
    }
    //sleep(5);
    quit=1;
    //pthread_cond_broadcast(&cond);
    for(int i=0;i<TNUM;i++)
    {
        pthread_join(Thread[i],nullptr);
    }

    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&cond);
    return 0;
} */
typedef void (*func)(std::string&name,pthread_mutex_t*mutex,pthread_cond_t*cond);
#define TNUM 5
pthread_t Thread[TNUM];
class ThreadData
{
public:
    ThreadData(const std::string&name,pthread_mutex_t*mutex,pthread_cond_t*cond,func task)
    :_name(name),_mutex(mutex),_cond(cond),_task(task)
    {}
public:
    std::string _name;
    pthread_mutex_t* _mutex;
    pthread_cond_t* _cond;
    func _task;
};
volatile int quit=0;
void start_routine1(std::string&name,pthread_mutex_t*mutex,pthread_cond_t*cond)
{
    while(!quit)
    {   
        //pthread_mutex_lock(mutex);//如果每一个线程都没有这个加锁与解锁操作，那么当因为pthread_cond_wait而陷入阻塞挂起的线程接收到通知被唤醒的时候，会将该mutex互斥锁归还给这个线程
        pthread_cond_wait(cond,mutex);//也就是说，第一个被唤醒的线程去申请这个mutex互斥锁时会申请成功并持有这个mutex互斥锁，但是由于这几个线程调用pthread_cond_wait都使用的是同一个mutex，那么随后被唤醒的线程
        cout<<name<<" is running"<<endl;//也都会去申请这个互斥锁，但是这个互斥锁被第一个唤醒的线程所占用，并且它也没有释放mutex锁的操作，所以这些线程会申请锁申请失败，也就是说将不会持有这个mutex互斥锁，
        //pthread_mutex_unlock(mutex);    //因此，这些个线程会继续阻塞在pthread_cond_wait这个位置。
        //sleep(20);
    }
    cout<<name<<"quit"<<endl;
}
void start_routine2(std::string&name,pthread_mutex_t*mutex,pthread_cond_t*cond)
{
    while(!quit)
    {   
        //pthread_mutex_lock(mutex);
        pthread_cond_wait(cond,mutex);
        cout<<name<<" is running"<<endl;
        //pthread_mutex_unlock(mutex);
        //sleep(20);
    }
    cout<<name<<"quit"<<endl;
}
void start_routine3(std::string&name,pthread_mutex_t*mutex,pthread_cond_t*cond)
{
    while(!quit)
    {   
        //pthread_mutex_lock(mutex);
        pthread_cond_wait(cond,mutex);
        cout<<name<<" is running"<<endl;
        //pthread_mutex_unlock(mutex);
        //sleep(20);
    }
    cout<<name<<"quit"<<endl;
}
void start_routine4(std::string&name,pthread_mutex_t*mutex,pthread_cond_t*cond)
{
    while(!quit)
    {
        //pthread_mutex_lock(mutex);
        pthread_cond_wait(cond,mutex);
        cout<<name<<" is running"<<endl;
        //pthread_mutex_unlock(mutex);
        //sleep(20);
    }
    cout<<name<<"quit"<<endl;
}
void start_routine5(std::string&name,pthread_mutex_t*mutex,pthread_cond_t*cond)
{
    while(!quit)
    {
        //pthread_mutex_lock(mutex);
        pthread_cond_wait(cond,mutex);
        cout<<name<<" is running"<<endl;
        //pthread_mutex_unlock(mutex);
        //leep(20);
    }
    cout<<name<<"quit"<<endl;
}
void* Entry(void*args)
{
    ThreadData*Th=(ThreadData*)args;
    Th->_task(Th->_name,Th->_mutex,Th->_cond);
}
int main()
{
    pthread_cond_t cond;
    pthread_mutex_t mutex;
    func func_n[TNUM]={start_routine1,start_routine2,start_routine3,start_routine4,start_routine5};
    pthread_mutex_init(&mutex,nullptr);
    pthread_cond_init(&cond,nullptr);
    for(int i=0;i<TNUM;i++)
    {
        ThreadData*Th=new ThreadData(std::string("Thread")+std::to_string(i+1),&mutex,&cond,func_n[i]);
        pthread_create(Thread+i,nullptr,Entry,(void*)Th);
        //sleep(1);
    }
    int count=10;
    sleep(3);
    while(count--)
    {
       pthread_cond_signal(&cond);//按照某种顺序把线程依次唤醒，使得各个线程都能得到调度
       sleep(1); 
    }
    sleep(5);
    quit=1;
    //pthread_cond_broadcast(&cond);
    int _count=TNUM;
    sleep(3);
    while(_count--)
    {
       pthread_cond_signal(&cond);//按照某种顺序把线程依次唤醒，使得各个线程都能得到调度
       sleep(1); 
    }
    sleep(30);
    for(int i=0;i<TNUM;i++)
    {
        pthread_join(Thread[i],nullptr);
    }

    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&cond);
    return 0;
}