#pragma once
//不多互斥锁进行封装，直接使用RAII的方式进行加锁
/* class LockGuard
{
public:
LockGuard(pthread_mutex_t*mutex):mutex_(mutex)
{
    pthread_mutex_lock(mutex_);
}
~LockGuard()
{
    pthread_mutex_unlock(mutex_);
}
private:
pthread_mutex_t* mutex_;

}; */
//对互斥锁进行封装，然后再使用RAII的方式进行加锁
class Mutex
{
public:
Mutex(pthread_mutex_t*mutex):mutex_(mutex)
{}
void Lock()
{
    pthread_mutex_lock(mutex_);
}
void UnLock()
{
    pthread_mutex_unlock(mutex_);
}
private:
pthread_mutex_t* mutex_;  
};
class LockGuard
{
public:
LockGuard(pthread_mutex_t*mutex):mutex_(mutex)
{
    mutex_.Lock();
}
~LockGuard()
{
    mutex_.UnLock();
}
private:
Mutex mutex_;
};