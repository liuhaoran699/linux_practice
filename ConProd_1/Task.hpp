#pragma once
typedef int (*function)(int,int);
class Task
{
public:
Task(){}
Task(int a,int b,function func):_a(a),_b(b),_func(func)
{}
int operator()(int a,int b)
{
    return _func(a,b);
}
function _func;
int _a;
int _b;
};
