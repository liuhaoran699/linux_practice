#include"BlockQueue.hpp"
#include"Task.hpp"
#include<iostream>
#include<pthread.h>
#include<unistd.h>
#include<cstdlib>
#include<ctime>
#define CONSUMER 5
#define PRODUCTOR 5
using std::endl;
using std::cout;
BlockQueue<Task>block_queue;
int add(int a,int b)
{
    return a+b;
}
void* Productor(void*args)
{
    srand(time(nullptr));
    while(1)
    {
        int a=rand()%10+1;
        int b=rand()%10+1;
        Task t(a,b,add);
        block_queue.push(t);
        cout<<pthread_self()<<"  派发任务 "<<a<<"+"<<b<<"=?"<<endl;
        sleep(2);
    }

}
void* Consumer(void*args)
{
    while(1)
    {
        Task t;
        block_queue.pop(&t);
        cout<<pthread_self()<<"  任务完成 "<<t._a<<"+"<<t._b<<"="<<t(t._a,t._b)<<endl;
    }
}
int main()
{
    pthread_t Consumer_[CONSUMER],Productor_[PRODUCTOR];

    /* pthread_t productor1, productor2, productor3,consumer;
    pthread_create(&productor1,nullptr,Productor,nullptr);
    //sleep(1);
    pthread_create(&productor2,nullptr,Productor,nullptr);
    //sleep(1);
    pthread_create(&productor3,nullptr,Productor,nullptr);
    //sleep(4);
    pthread_create(&consumer,nullptr,Consumer,nullptr); */
    for(int i=0;i<CONSUMER;i++)
    {
        pthread_create(Consumer_+i,nullptr,Consumer,nullptr);
    }
    for(int i=0;i<PRODUCTOR;i++)
    {
        pthread_create(Productor_+i,nullptr,Productor,nullptr);
    }
    for(int i=0;i<CONSUMER;i++)
    {
        pthread_join(*(Consumer_+i),nullptr);
    }
    for(int i=0;i<PRODUCTOR;i++)
    {
        pthread_join(*(Productor_+i),nullptr);
    }
    //sleep(30);
    /* pthread_join(productor1,nullptr);
    pthread_join(consumer,nullptr); */
    return 0;
}