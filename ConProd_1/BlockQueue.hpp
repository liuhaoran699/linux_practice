#pragma once
#include<queue>
#include<pthread.h>
#include"LockGuard.hpp"
#define CAPACITY 5
template<typename Task>
class BlockQueue
{
private:
bool isempty()
{
    return task_.size()==0;
}
bool isfull()
{
    return task_.size()==_capacity;
}
public:
BlockQueue(int capacity=CAPACITY):_capacity(capacity)
{
    pthread_mutex_init(&_mutex,nullptr);
    pthread_cond_init(&_empty,nullptr);
    pthread_cond_init(&_full,nullptr);
}
~BlockQueue()
{
    pthread_mutex_destroy(&_mutex);
    pthread_cond_destroy(&_empty);
    pthread_cond_destroy(&_full);
}
void push(const Task&task)
{
    LockGuard mutex(&_mutex);//RAII风格加锁,该对象初始化调用构造函数进行加锁，该对象出函数自动调用析构函数进行解锁操作
    //pthread_mutex_lock(&_mutex);                     //可能存在pthread_cond_wait接口调用失败的情况，使用while判断，即使调用失败，也会因为循环的再次进入从而继续调用pthread_cond_wait进行等待
    while(isfull())pthread_cond_wait(&_full,&_mutex);//可能存在被伪唤醒的情况，也就是被唤醒以后条件仍然是不满足的，使用while判断，即使被伪唤醒，再次循环判断条件不满足，继续wait
                   //当处于阻塞挂起等待以后，传入的互斥锁会被自动释放，当处于阻塞等待状态时被唤醒后，pthread_cond_wait会帮我们获得因为调用该接口而被释放的锁                      
    task_.push(task);
    pthread_cond_signal(&_empty);
    //pthread_mutex_unlock(&_mutex);
}
void pop(Task*task)
{
    LockGuard mutex(&_mutex);
    //pthread_mutex_lock(&_mutex);
    while(isempty())pthread_cond_wait(&_empty,&_mutex);
    *task=task_.front();
    task_.pop();
    pthread_cond_signal(&_full);//调用该接口的线程会在该线程调用成功pthread_mutex_unlock以后将互斥锁给被pthread_cond_signal唤醒的线程
    //pthread_mutex_unlock(&_mutex);
}
private:
std::queue<Task>task_;
int _capacity;
pthread_mutex_t _mutex;//这把锁既能够维护生产者与生产者之间的互斥关系，也能维护生产者与消费者之间的互斥关系，还能够维持消费者和消费者之间的互斥关系
pthread_cond_t _empty;
pthread_cond_t _full;
};