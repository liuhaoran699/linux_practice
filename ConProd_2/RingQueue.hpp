#pragma once
#include"Task.hpp" 
#include"Sem.hpp"
#include"Lock.hpp"
using std::cout;
using std::endl;
int default_size=4;



//多线程的生产者的意义：虽然生产者生产任务到仓库(这里的循环队列)是串行的，看起来没什么效率，但是生产者从外界拿取任务是并行的，从外界拿取任务也是要耗费时间的，多线程并行地从外界拿取任务是能够提高效率的
//多线程的消费者的意义：虽然消费者从仓库(这里的循环队列)拿取任务是串行的，看起来没什么效率，但是消费者处理拿到的任务是并行的，从消费者处理拿取到的任务也是要耗费时间的，多线程并行地处理任务是能够提高效率的
//所以多线程的生产者和消费者的最大意义不是在于在仓库中放置和拿取任务，而是在于我们可以并发地去生产和处理任务

//信号量的本质：信号量的本质是一个计数器，这个计数器可以让我们不必进入临界区就可以得知资源的情况，甚至可以减少在临界区内部的判断
//比如之前写的生产者与消费者模型，我们是按照 加锁->判断与访问->解锁 的流程来进行生产和消费的，我们是因为不知道临界资源的情况，所以需要在加锁内部做临界资源条件是否满足的判断来决定对临界资源的处理方式
//使用信号量，要提前预设信号量的情况，因此在这之后的pv操作(pv操作是原子的)中，我们能得知信号量是多少以及信号量的变化情况从而得知临界资源的情况
template<typename Task>
class RingQueue
{
public:
RingQueue(int size=default_size):_size(size),rq(size),p_step(0),c_step(0),sem_space(size),sem_sourse(0)
{}
void pop(Task*task)
{
    sem_sourse.p();//信号量的申请不放在锁内是为了保证多线程对信号量的申请是并行的，如果上锁再申请信号量，那么将会导致多线程对信号量的申请为串行，由于信号量的申请和释放都是原子的，所以不需要是串行，串行反而降低效率
    GuardLock lock(&c_mutex);//消费者线程竞争锁，保证消费者之间的互斥
    *task=rq[c_step++];
    c_step%=_size;
    sem_space.v();
}
void push(const Task&task)
{
    sem_space.p();
    GuardLock lock(&p_mutex);//生产者线程竞争锁，保证生产者之间的互斥
    rq[p_step++]=task;
    p_step%=_size;
    sem_sourse.v();
}
private:
std::vector<Task>rq;
int _size;
int c_step;
int p_step;
Sem sem_space;
Sem sem_sourse;
Mutex c_mutex;
Mutex p_mutex;
};