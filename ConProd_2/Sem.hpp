class Sem
{
public:
Sem(int size)
{
    sem_init(&sem,0,size);

}
~Sem()
{
    sem_destroy(&sem);
}
void p()
{
    sem_wait(&sem);
}
void v()
{
    sem_post(&sem);
}
private:
sem_t sem;
};