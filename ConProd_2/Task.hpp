#pragma once

class Task
{
typedef int(*func)(int,int);
public:
Task()
{}
Task(int a,int b,func function):_a(a),_b(b),_function(function)
{}
int operator()()
{
    return _function(_a,_b);
}
int get_first()
{
    return _a;
}
int get_second()
{
    return _b;
}
private:
int _a;
int _b;
func _function;
};