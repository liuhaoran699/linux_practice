#include"Socket.hpp"
#include<string>
#include<cstring>
#include<cerrno>
#include<poll.h>
#define NON_FD -1 //NON_FD表示没有对应的fd
class PollServer{
public:
    PollServer(int pollnum):server_ip("0.0.0.0"),server_port("8080"),PollNum(pollnum)
    {
        listen_socket.Bind(server_port);
        listen_socket.Listen();
        poll_fds=new struct pollfd[PollNum];
        //第一个位置留给监听套接字
         poll_fds[0].fd=listen_socket.socket_;
         poll_fds[0].events=POLLIN;
         //剩下的全初始化，默认初始时每个位置都没有对应的fd
        for(int i=1;i<PollNum;i++)
        {
            poll_fds[i].fd=NON_FD;
            poll_fds[i].events= poll_fds[i].revents=0;
        }
    }
    void Start()
    {
        while(true)
        {
            std::cout<<"存在的socket: ";
            for(int i=0;i<PollNum;i++)
            {
                if(poll_fds[i].fd==NON_FD)continue;
                std::cout<<poll_fds[i].fd<<" ";
            }
            std::cout<<std::endl;
            int p_ret=poll(poll_fds,PollNum,-1);//如果对应的fd是非法的fd，该接口不会处理这些fd
            if(p_ret<0)
            {
                message(WARNING,"poll发生错误,errno:%d,错误原因:%s",errno,strerror(errno));
                continue;
            }
            else if(p_ret==0)
            {
                message(NORMAL,"没有IO任务就绪");
            }
            else{
                //存在有IO任务就绪,处理这些IO任务
                HanderIO();
            }

        }
    }
    ~PollServer()
    {
        close(listen_socket.socket_);
        delete[]poll_fds;
    }
private:
    void HanderIO()
    {
        if(poll_fds[0].revents&POLLIN)HanderToListen();
        for(int i=1;i<PollNum;i++)
        {
            if(poll_fds[i].fd==-1)continue;
            if(poll_fds[i].revents&POLLIN)HanderToClient(poll_fds[i].fd);
        }
    }
   void HanderToListen()
    {
        std::string client_ip;
        uint16_t client_port;
        int ClientFd=listen_socket.Accept(&client_ip,&client_port);
        int pos=1;
        for(;pos<PollNum;pos++)
        {
            if(poll_fds[pos].fd!=NON_FD)continue;
            poll_fds[pos].fd=ClientFd;
            poll_fds[pos].events=POLLIN;
            break;
        }
        if(pos==PollNum)
        {
            message(WARNING,"pollfd类型数组已满,无法为更多的套接字提供IO管理");
            close(ClientFd);
        }
    }
    void HanderToClient(int ClientFd)
    {
        char buff[1024];
        ssize_t s=recv(ClientFd,buff,sizeof(buff)-1,0);
        if(s>0)
        {
            buff[s]=0;
            std::cout<<"["<<ClientFd<<"号客户端]says:"<<buff;
        }
        else if(s==0)
        {
            message(NORMAL,"Client断开连接，我也断开连接");
            close(ClientFd);
            for(int i=1;i<PollNum;i++)
            {
                if(poll_fds[i].fd==ClientFd)
                {
                    poll_fds[i].fd=NON_FD;
                    break;
                }
            }
        }
        else{
            message(WARNING,"recv调用发生错误,errno:%d,错误原因:%s",errno,strerror(errno));
        }
    }
private:
int PollNum;
struct pollfd*poll_fds;
Socket listen_socket;
std::string server_ip;
std::string server_port;
};