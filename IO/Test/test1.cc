#include<iostream>
#include<unistd.h>
#include<fcntl.h>
#include<cerrno>
#include<cstring>
int main()
{
  char buff[100];
  int flag=fcntl(0,F_GETFL);//获取0号文件描述符对应的状态标记位(也就是对应open时的flags参数) (cmd参数设置为F_GETFL)
  fcntl(0,F_SETFL,flag|O_NONBLOCK);//再原有标记位的基础上增添O_NONBLOCK标记位，并对0号文件描述符对应状态标记位进行设置(cmd参数为F_SETFL)
  while(true)
  {
   errno=0; //为了让每次循环读取时errno表明的是当前read的错误原因，将errno置为0，表示初始时没有错误
   ssize_t s=read(0,buff,sizeof(buff)-1);
   if(s<0)
   {
     //由于s<0可能是因为当前没有资源所造成的，把出现错误和当前文件描述符没有资源这两种情况分开看待
     //使用errno来区别s<0的原因(因为s<0表示出错，出错errno会被设置,表明错误原因)
     //errno为11即表示资源未就绪，对应的宏也就是EWOULDBLOCK
    if(errno==EWOULDBLOCK)//资源未就绪
    {
      std::cout<<"资源未就绪,请再试一试"<<std::endl;
      continue;
      
    }
    else if(errno==EINTR)
    {
      std::cout<<"当前read被信号中断,请再试一试"<<std::endl;
      continue;
    }
    else{
      //出现了错误
     std::cout<<"read error"<<"错误码"<<errno<<"错误原因:"<<strerror(errno)<<std::endl;
    }
    sleep(1);
   }
   else{
     buff[s-1]=0;//因为read会读入将最后的回车'\n',所以将这个'\n'位置设置为'\0';
     std::cout<<"["<<strerror(errno)<<"]"<<"echo:"<<buff<<std::endl;
   }
  }
  return 0;
}
