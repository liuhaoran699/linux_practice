#pragma once
#include <string>
#include <vector>
#define SEP "X"
#define SEPLENTH strlen(SEP)
#define SPACE " "
#define SPACE_LEN strlen(SPACE)
void CutString(std::string &s, std::vector<std::string> &out)
{
    while (true)
    {
        int pos = s.find(SEP);
        if (pos == std::string::npos)
            break;
        out.push_back(s.substr(0, pos));
        s.erase(0, pos + SEPLENTH);
    }
}

class NetCalRequest
{
public:
    bool Deserialize(std::string s)
    {
        int first = s.find(SPACE);
        if (first == std::string::npos)
            return false;
        x = std::stoi(s.substr(0, first));
        op = s[first + SPACE_LEN];
        int second = s.find(SPACE, first + SPACE_LEN);
        if (second == std::string::npos)
            return false;
        y = std::stoi(s.substr(second + SPACE_LEN));
        return true;
    }

    int x;
    char op;
    int y;
};
class NetCalResponce
{
public:
    std::string Serialization()
    {
        std::string s;
        s+=std::to_string(state);
        s+=SPACE;
        s+=std::to_string(res);
        return s;
    }
    bool state;
    int res;
};
NetCalResponce CalRet(const NetCalRequest &request)
{
    int x = request.x;
    int y = request.y;
    char op = request.op;
    int res;
    std::cout << "result:";
    NetCalResponce responce;
    switch (op)
    {
    case '+':
        res = x + y;
        std::cout << x << op << y << "=" << res << std::endl;
        responce.state = true;
        responce.res = res;
        break;
    case '-':
        res = x - y;
        std::cout << x << op << y << "=" << res << std::endl;
        responce.state = true;
        responce.res = res;
        break;
    case '*':
        res = x * y;
        std::cout << x << op << y << "=" << res << std::endl;
        responce.state = true;
        responce.res = res;
        break;
    case '/':
        res = x / y;
        std::cout << x << op << y << "=" << res << std::endl;
        responce.state = true;
        responce.res = res;
        break;
    default:
        responce.state = false;
        responce.res = -1;
        std::cout << "error!" << std::endl;
    }
    return responce;
}