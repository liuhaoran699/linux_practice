#include"Log.hpp"
#include"Server.hpp"
#include"Protocol.hpp"
#include<string>
#include<memory>
void add(Connection*back,const std::string& equation)//back参数的作用是调用Server内暴露出的接口，从而通知epoll关心需要响应的socket的写事件，从而将响应发送给客户端
{
    //反序列化出request
    NetCalRequest request;
    request.Deserialize(equation);
    //计算并构建响应
    NetCalResponce responce=CalRet(request);
    //序列化响应
    std::string responce_=responce.Serialization();
    //让服务器帮我们发送响应
    std::cout<<responce_<<std::endl;
    back->outbuffer+=responce_;
    back->back->EpollCltl(back,true);
    
}
int main()
{
    std::unique_ptr<Server>server(new Server);
    server->Start(add);
    return 0;
}