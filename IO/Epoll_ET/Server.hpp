#pragma once
#include "Socket.hpp"
#include "Protocol.hpp"
#include <sys/epoll.h>
#include "Epoll.hpp"
#include <cstdlib>
#include <cerrno>
#include <cstring>
#include <string>
#include <functional>
#include <unordered_map>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <vector>
class Server;
class Connection;
using Task = std::function<void(Connection *, const std::string &)>;
using func_t = std::function<void(int)>;
class Connection
{
public:
    // 每个socket都有一个Connection与之对应，Connection负责对IO数据通过回调进行处理

    // 设置回调函数的接口，
    void SetCallBack(func_t Read, func_t Write, func_t Except)
    {
        Read_cb = Read;
        Write_cb = Write;
        Except_cb = Except;
    }

    // 回调函数，当读，写，异常事件发生，就调用这些函数来进行相关处理//TODO
    // 由于ET模式是提醒一次的特性，所以这些回调函数必须一次将输入缓冲区中的数据读走，或者一次就将发送缓冲区写满
    func_t Read_cb;
    func_t Write_cb;
    func_t Except_cb;

    // 存放读取上来的数据
    std::string inbuffer;
    // 存放要发送的数据
    std::string outbuffer;
    // 用于在具体业务处理完成后调用Server内部暴露出的接口，从而设置对应需要发回数据的socket为关心写事件(因为默认只关心读事件)，托管给epoll处理
    Server *back;
    // 用于告知业务层，该响应是针对哪个socket的，从而通过设置epoll来让epoll关心该socket的写事件
    int fd;


    //还可以设置一些字段来处理长时间未与服务端进行交互的连接
    //通过时间戳来记录连接最后一次活动的时间是什么
};
class Server
{

    static int EventArrSize;

public:
    Server()
    {
        listen_socket.Bind("8080");
        listen_socket.Listen();
        epfd = Epoll::EpollCreaate();
        if (epfd < 0)
            exit(4);
        // epoll模型创建成功
        message(NORMAL, "Epoll模型创建成功");
        ReadyEvents = new epoll_event[EventArrSize]; // 该数组用于获取epoll查找到的就绪的fd
        Connect(listen_socket.socket_, std::bind(&Server::Accept, this, std::placeholders::_1), nullptr, nullptr);
        // Fcntl(listen_socket.socket_);//设置listen套接字为非阻塞
    }

private:
    void Connect(int fd, func_t r, func_t w, func_t except) // 负责建立fd与Conection的映射
    {
        // 建立套接字与Connection的映射，Connection负责对IO数据通过回调进行处理
        Connection *connection = new Connection;
        connection->fd = fd;
        connection->back = this;
        connection->SetCallBack(r, w, except);
        FdToConnection.insert(std::make_pair(fd, connection));
        // 设置套接字为非阻塞
        int flag = fcntl(fd, F_GETFL);
        fcntl(fd, F_SETFL, flag | O_NONBLOCK);
        // 将套接字托管给epoll模型
        Epoll::EpollCtl(epfd, EPOLL_CTL_ADD, fd, EPOLLIN | EPOLLET); // 任何多路转接的服务器一般默认都只会打开对读取事件的关心，对于写事件的关心是按照需求来进行设置(只要写需要时才打开，其余时候都是关闭)
                                                                     // 如果发送缓冲区默认设置为关心，由于发送缓冲区中开始时一定是空间就绪的，所以如果代码没控制好，可能导致将还未处理完的数据发送回去，所以服务器默认打开读，其他按需设置.

        std::cout << "[" << fd << "号套接字]初始设置成功" << std::endl;
    }

public:
    void Start(Task _task)
    {
        task = _task;
        while (true)
        {
            int ret = Epoll::EpollWait(epfd, ReadyEvents);
            if (ret < 0)
                message(ERROR, "EpollWait发生错误,errno:%d,reason:", errno, strerror(errno));
            for (int i = 0; i < ret; i++)
                Hander(i);
        }
    }
    bool EpollCltl(Connection *conn, bool _write, bool _read = true)
    {
        uint32_t events = (_write ? EPOLLOUT : 0) | (_read ? EPOLLIN : 0);
        bool res = Epoll::EpollCtl(epfd, EPOLL_CTL_MOD, conn->fd, events);
        return res;
    }

private:
    void Hander(int pos) // 不用区分事件时listen套接字的时间还是普通套接字的事件，因为每个套接字都注册了对应的处理方法，我们只需要在相应条件就绪时调用这些套接字注册的对应方法即可
    {
        int socket = ReadyEvents[pos].data.fd;
        uint32_t events = ReadyEvents[pos].events;
        //1. 如果socket的读写事件都就绪了，我们先判断它的读事件是否就绪，然后进行处理读事件。通过读事件，发现对方已经断开连接，所以我们将该连接状态从epoll中移除托管，关闭套接字，并且将其从
        //   std::unordered_map<int,Connection*>FdToConnection中移除。然后再进行处理写事件，此时该连接已经不存在，所以虽然之前写事件是就绪的，但是此时没有必要再进行写入操作。所以在这里
        //   我们需要加上连接是否存在的判断
        //   如果该套接字存在，并且该套接字对应事件对应事件就绪，那么就通过这些套接字事先注册好的回调函数去处理对应的事件。
        //2. 如果对应的文件描述符发生错误，那么对应fd会被epoll返回就绪，并且events被设置EPOLLERR;
        //   如果对应的文件描述符被挂断(也就是对方断开连接)，那么对应fd会被epoll返回就绪，并且events被设置EPOLLHUP;
        //   那么也就是说进入Hander这个函数，可能是因为对应fd出现了上面所说的问题，那么我们应该对这些情况做出处理不然由于这些fd并不是处于EPOLLIN或者EPOLLOUT而导致什么也不做:
        //   如果出现这样的情况，就手动将events设置为EPOLLIN和EPOLLOUT，这样就能够进行后面的判断语句，从而以EPOLLIN的情况进行处理，但是由于本来这些fd就是出现了异常，所以实际上
        //   在以EPOLLIN的情况进行处理的时候就能立即判断出发现异常，从而跳转到对应设置的异常处理函数
        //   这样做的话，就能够将异常统一交给Read_cb和Write_cb，进而交给Except_cb来进行处理
        if(events&EPOLLERR)events|=(EPOLLIN|EPOLLOUT);
        if(events&EPOLLHUP)events|=(EPOLLIN|EPOLLOUT);


        if (ConnectExist(socket) && (events & EPOLLIN) && FdToConnection[socket]->Read_cb != nullptr)
            FdToConnection[socket]->Read_cb(socket);
        if (ConnectExist(socket) && (events & EPOLLOUT) && FdToConnection[socket]->Write_cb != nullptr)
            FdToConnection[socket]->Write_cb(socket);
    }
    bool ConnectExist(int socket)
    {
        auto iter = FdToConnection.find(socket);
        if (iter == FdToConnection.end())
            return false;
        else
            return true;
    }
    /* void Accepter(int pos)
    {
        if((ReadyEvents[pos].events&EPOLLIN)&&FdToConnection[ReadyEvents[pos].data.fd]->Read_cb!=nullptr)
        FdToConnection[ReadyEvents[pos].data.fd]->Read_cb(ReadyEvents[pos].data.fd);
    } */
    void Accept(int fd)
    {
        while (true)
        {
            int AcceptErrno = 0;
            int ClientFd = Socket(fd).Accept(nullptr, nullptr, &AcceptErrno);
            if (ClientFd < 0)
            {
                if (AcceptErrno == EAGAIN || AcceptErrno == EWOULDBLOCK) // 套接字读取为非阻塞状态，此时套接字中没有内容，表明本次连接已全部获取
                {
                    message(NORMAL, "listen套接字中的连接状态已全部获取");
                    break;
                }
                else if (AcceptErrno == EINTR)
                    continue; // accept调用被信号中断(概率非常低)，重新进行accept
                else
                {
                    message(ERROR, "accept error, errno:%d,reason:%s", errno, strerror(errno));
                    break;
                }
            }
            else
                Connect(ClientFd,
                        std::bind(&Server::Reader, this, std::placeholders::_1),
                        std::bind(&Server::Writer, this, std::placeholders::_1),
                        std::bind(&Server::Excepter, this, std::placeholders::_1));
        }
    }
    void Reader(int fd)
    {
        //std::cout << "Reader Start" << std::endl;
        std::string &inbuffer = FdToConnection[fd]->inbuffer;
        while (true)
        {
            std::cout << inbuffer << std::endl;
            char buff[1024];
            ssize_t s = recv(fd, buff, sizeof(buff) - 1, 0);
            //std::cout << "s:" << s << std::endl;
            if (s < 0)
            {
                //std::cout << "errno" << errno << "  reason:" << strerror(errno) << std::endl;
                if (errno == EAGAIN || errno == EWOULDBLOCK)
                    break;
                else if (errno == EINTR)
                    continue;
                else
                {
                    message(WARNING, "recv error, errno:%d,reason:%s", errno, strerror(errno));
                    FdToConnection[fd]->Except_cb(fd); // 该套接字初一出异常了，所以交给该套接字注册的Except_cb回调函数来处理
                    break;
                }
            }
            else if (s == 0)
            {
                // 对方断开连接
                // 1.将该socket从epoll中移除
                Epoll::EpollCtl(epfd, EPOLL_CTL_DEL, fd, 0);
                // 2.将该socket对应的Connection释放掉
                delete FdToConnection[fd];
                // 3.将该socket在FdToConnection中的映射关心移除
                FdToConnection.erase(fd);
                // 4.关闭该socket
                close(fd);
                break;
            }
            else
            {
                // std::cout<<"buff:"<<buff[s-1]<<std::endl; ---  s-1位置处也是'\0',也就是说发送的字节会默认加上\0，但是为了保险起见，还是在buffer[s]的位置设置为'\0'
                buff[s] = 0;
                inbuffer += buff;
            }
        }
       // std::cout << "数据全部读取完毕" << std::endl;
        // 自此，如果recv过程中没有出错，那么现在接收缓冲区中的数据已经被全部读取完,存储在inbuffer里
        // 通过协议拆分出完整的报文
        std::vector<std::string> CompletePackage;
        CutString(inbuffer, CompletePackage);
       // std::cout << "CompletePackageSize:" << CompletePackage.size() << std::endl;
        for (auto &s : CompletePackage) // 当然我们可以在这里将s进行封装成task，然后放置到任务队列里，任务处理交给后端线程池,然后server服务器就可以直接不管了，去处理其他的事务，而不是服务器主进程去处理这些具体业务
        {
            std::cout << "[" << fd << "号客户端]请求:" << s << std::endl;
            task(FdToConnection[fd], s);
        }
    }
    void Writer(int fd)
    {
        Connection *_connect = FdToConnection[fd];
        while (true)
        {
            ssize_t s = send(fd, _connect->outbuffer.c_str(), _connect->outbuffer.size(), 0);
            if (s < 0)
            {
                if (errno == EAGAIN || errno == EWOULDBLOCK)
                    break; // 由于套接字被设置为非阻塞式等待，所以当发送缓冲区满了便会直接返回，返回值为-1，且errno==EAGAIN||errno==EWOULDBLOCK
                else if (errno == EINTR)
                    continue;
                else
                {
                    message(WARNING, "send error ,errno:%d,reason:%s", errno, strerror(errno));
                    _connect->Except_cb(fd);
                    break;
                }
            }
            else if (s == 0) // 数据发送完毕,跳出循环
            {
                break;
            }
            else
            {
                _connect->outbuffer.erase(0, s);
            }
        }
        // 走到这里，数据可能还没有发送完，有可能发送缓冲区满了，但是数据并没有发送完，在这种情况下，就应该继续保持关心该套接字的写事件，否则就应该关闭该套接字的写事件
        if (_connect->outbuffer.size() == 0)
            EpollCltl(_connect, false);
    }
    void Excepter(int fd)
    {
        if(!ConnectExist(fd))return;
        // 1.将该socket从epoll中移除
        Epoll::EpollCtl(epfd, EPOLL_CTL_DEL, fd, 0);
        // 2.将该socket对应的Connection释放掉
        delete FdToConnection[fd];
        // 3.将该socket在FdToConnection中的映射关系移除
        FdToConnection.erase(fd);
        // 4.关闭该socket
        close(fd);
    }

private:
    int epfd;
    struct epoll_event *ReadyEvents;
    Socket listen_socket;
    std::unordered_map<int, Connection *> FdToConnection;
    Task task;
};
int Server::EventArrSize = 500;