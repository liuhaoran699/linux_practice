#include<sys/epoll.h>
#include<cstdlib>
class Epoll{
private:
static const int max_event_num=500;
static const int _timeout=1000;
public:
static int EpollCreaate()
{
    int epfd=epoll_create(255);
    return epfd;
}
static int EpollCtl(int epfd,int operate,int fd,uint32_t events)
{
    struct epoll_event event;
    event.data.fd=fd;
    event.events=events;
    int ret=epoll_ctl(epfd,operate,fd,&event);
    return ret;
}
static int EpollWait(int epfd,struct epoll_event* events)
{
    int ReadyFd=epoll_wait(epfd,events,max_event_num,_timeout);
    return ReadyFd;
}
};