#include<sys/epoll.h>
#include<cstdlib>
#include"Log.hpp"
class Epoll{
public:
static int EpollCreate()
{
    int epfd=epoll_create(256);//这个值只要大于0就行
    if(epfd<0)exit(4);
    return epfd;
}
static bool EpollOper(int epfd,int operate,int fd,uint32_t events)
{
    struct epoll_event _events;
    _events.events=events;
    _events.data.fd=fd;
    // 如果底层就绪的sock非常多，revs承装不下，那么可以分批次拿取，一次拿不完，分多次拿。
    //epoll_wait的返回值:有几个fd上的事件就绪，就返回几，epoll返回的时候，会将所有
    //就绪的event按照顺序(从数组0号下标开始以此往后放)放入到revs数组中，然后放入返回值个event到这个数组中
    int EpOper=epoll_ctl(epfd,operate,fd,&_events);
    if(EpOper<0)return false;
    return true;   
}
static int EpollWait(int epfd,struct epoll_event*events,int maxevents,int timeout)
{
    int FdReady=epoll_wait(epfd,events,maxevents,timeout);
    return FdReady;
}
};