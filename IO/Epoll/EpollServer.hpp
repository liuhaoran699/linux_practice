#include"Socket.hpp"
#include"Log.hpp"
#include"Epoll.hpp"
#include<cerrno>
#include<string>
class EpollServer{
    static const int EventNum=500;
    static const int EventOnceNum=50;
    static const int timeout=-1;
public:
    EpollServer()
    {
        listen_socket.Bind("8080");
        listen_socket.Listen();
        epfd=Epoll::EpollCreate();
        ep_event_ptr=new struct epoll_event[EventNum];
        bool EpOp=Epoll::EpollOper(epfd,EPOLL_CTL_ADD,listen_socket.socket_,EPOLLIN);
        if(!EpOp)exit(5);
    }
    void Start()
    {
        while(true)
        {
            //让epoll去等待fd就绪,在做读取数据的操作
            int ReadyFd=Epoll::EpollWait(epfd,ep_event_ptr,EventOnceNum,timeout);
            if(ReadyFd<0)
            {
                message(WARNING,"EpollWait Error,errno:%d,error reason:%s",errno,strerror(errno));
                continue;
            }
            else{
                HandlerFd(ReadyFd);
            }
            
        }
    }
     ~EpollServer()
       {
        close(listen_socket.socket_);
        delete[]ep_event_ptr;
       }
private:
    void HandlerFd(int FdNum)
    {
        for(int i=0;i<FdNum;i++)
        {
            if(ep_event_ptr[i].data.fd==listen_socket.socket_)Accepter();
            else Recver(ep_event_ptr[i].data.fd);
        }
    }
    void Accepter()
    {
        std::string ClientIp;
        uint16_t ClientPort;
        int ClientFd=listen_socket.Accept(&ClientIp,&ClientPort);
        if(ClientFd<0)return;
        bool EpOp=Epoll::EpollOper(epfd,EPOLL_CTL_ADD,ClientFd,EPOLLIN);
        if(!EpOp)
        {
            message(WARNING,"Epoll::EpollOper Error,errno:%d,error reason:%s",errno,strerror(errno));
            close(ClientFd);
        }


    }
    void Recver(int fd)
    {
       char buff[1024];
       ssize_t s=recv(fd,buff,sizeof(buff)-1,0);
       if(s<0)
       {
        message(WARNING,"recv error,errno:%d,error reason:%s",errno,strerror(errno));
       } 
       else if(s==0)
       {
        std::cout<<"["<<fd<<"号客户端]断开连接,服务端也断开与他的连接"<<std::endl;
         close(fd);
        // 1. 先在epoll中去掉对sock的关心
        Epoll::EpollOper(epfd,EPOLL_CTL_DEL,fd,0);
        // 2. 再close文件
        //close(fd);
        //因为要对epoll中的文件描述符做操作时，前提要保证该文件描述符是合法有效的文件描述符，
        //所以如果先close这个文件描述符，这个文件描述符就变成非法的文件描述符了，再调用epoll_ctl进行操作时，可能会报错
       }
       else{
        buff[s]=0;
        std::cout<<"["<<fd<<"号客户端]says:"<<buff<<std::endl;
       }
      

    }
private:
struct epoll_event*ep_event_ptr;
int epfd;
Socket listen_socket;
};