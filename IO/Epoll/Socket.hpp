#pragma once
#include"Log.hpp"
#include<cstring>
#include<cerrno>
#include<sys/socket.h>
#include<sys/types.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include<unistd.h>
#include<cstdlib>
#include<cstdio>
#include<string>
#include<iostream>
#define SIZE 1024
#define BACKLOG 1

class Socket
{
public:
Socket(int socket_id=-1):socket_(socket_id)
{
    if(socket_id==-1)
    {
        int so=socket(AF_INET,SOCK_STREAM,0);
        if(so==-1)
        {
            message(FATAL,"错误码：%d  socket:%s",errno,strerror(errno));
            exit(1);
        }
        message(NORMAL,"套接字成功");
        socket_=so;
    }
}
void Bind(std::string port,const std::string&ip="0.0.0.0")
{
    sockaddr_in server;
    memset(&server,0,sizeof server);
    server.sin_family=AF_INET;
    server.sin_addr.s_addr=inet_addr(ip.c_str());
    server.sin_port=htons((uint16_t)std::stoi(port));
    int _bind=bind(socket_,(sockaddr*)&server,sizeof server);
    if(_bind==-1)
    {
        message(FATAL,"错误码：%d  bind:%s",errno,strerror(errno));
        exit(2);
    }
    message(NORMAL,"bind成功");
}
void Listen()
{
    int _listen=listen(socket_,BACKLOG);
    if(_listen==-1)
    {
        message(FATAL,"错误码：%d  listen:%s",errno,strerror(errno));
        exit(3);
    }
     message(NORMAL,"监听状态设置成功");
}
int Accept(std::string*ip,uint16_t*port)
{
    sockaddr_in client;
    socklen_t clisize=sizeof client;
    int _accept=accept(socket_,(sockaddr*)&client,&clisize);
    if(_accept==-1)
    {
        message(ERROR,"错误码：%d  accept:%s",errno,strerror(errno));
        return -1;
    }
     message(NORMAL,"accept成功");
    if(ip)*ip=inet_ntoa(client.sin_addr);
    if(port)*port=client.sin_port;
    return _accept;

}
int Connect(const std::string& ip,const std::string&port)
{
    sockaddr_in server;
    memset(&server,0,sizeof server);
    server.sin_family=AF_INET;
    server.sin_addr.s_addr=inet_addr(ip.c_str());
    server.sin_port=htons((uint16_t)std::stoi(port));
    std::cout<<ip<<"  "<<port<<std::endl;
    int _connect=connect(socket_,(sockaddr*)&server,sizeof server);
    if(_connect==-1)
    {
        message(ERROR,"错误码：%d  connect:%s",errno,strerror(errno));
        return -1;
    }
    message(NORMAL,"connect成功");
    return 0;
}


public:
int socket_;
};
