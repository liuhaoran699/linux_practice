#include "Socket.hpp"
#include <string>
#include<cstring>
#include <sys/select.h>
#include<cerrno>
#define NUM sizeof(fd_set)*8
class SelectServer
{
public:
    SelectServer(std::string port, std::string ip = "0.0.0.0") : server_port(port), server_ip(ip)
    {
        listen_socket.Bind(server_port, server_ip);
        listen_socket.Listen();
        for(int i=0;i<NUM;i++)
        {
            ReadFdArray[i]=-1;//初始时没有任何文件描述符在这个读事件数组中
        }
         ReadFdArray[0]=listen_socket.socket_;//规定0号下标位存储的是监听套接字
    }
    void start()
    {
        while (true)
        {
            FD_ZERO(&readset);
            //FD_SET(listen_socket.socket_, &readset);
            int MaxReadFd=listen_socket.socket_;
            for(int i=0;i<NUM;i++)
            {
                if(ReadFdArray[i]==-1)continue;
                FD_SET(ReadFdArray[i],&readset);
                if(ReadFdArray[i]>MaxReadFd)MaxReadFd=ReadFdArray[i];
            }
            std::cout<<"RecordFd:";
            for(int i=0;i<NUM;i++)
            {
                if(ReadFdArray[i]==-1)continue;
                std::cout<<ReadFdArray[i]<<" ";
            }
            std::cout<<std::endl;
            std::cout<<"MaxReadFd:"<<MaxReadFd<<std::endl;
            //int ReadyFd=select(listen_socket.socket_+1,&readset,nullptr,nullptr,nullptr);
            int ReadyFd=select(MaxReadFd+1,&readset,nullptr,nullptr,nullptr);
            if(ReadyFd<0)
            {
                message(WARNING,"errno:%d,error reason:%s",errno,strerror(errno));
                continue;
            }
            else{
                Hander();
            }
        }
    }
    ~SelectServer()
    {
        close(listen_socket.socket_);
    }
private:
    void Hander()
    {
        if(FD_ISSET(listen_socket.socket_,&readset))//监听套接字下有连接
        {
           HanderTolisten();

        }
        for(int i=1;i<NUM;i++)
        {
            if(ReadFdArray[i]==-1)continue;
            if(FD_ISSET(ReadFdArray[i],&readset))HanderToClient(ReadFdArray[i]);
        }
    }
    void HanderTolisten()
    {
         std::string client_ip;
            uint16_t client_port;
            int client_fd=listen_socket.Accept(&client_ip,&client_port);//此时不用等待，直接获取
            int pos=1;
            for(;pos<NUM;pos++)
            {
                if(ReadFdArray[pos]!=-1)continue;
                ReadFdArray[pos]=client_fd;
                break;
            }
            if(pos==NUM)
            {
                //此时已经对于fd_set位图，已经无力维护更多的文件描述符,所以关闭连接
                close(client_fd);
            }
    }
    void HanderToClient(int ClientFd)
    {
        char buff[1024];
        ssize_t s=recv(ClientFd,buff,sizeof(buff)-1,0);
        if(s>0)
        {
            //接收到数据消息
            buff[s]=0;
            std::cout<<"["<<ClientFd<<"号客户端]发起的请求:"<<buff<<std::endl;
        }
        else if(s==0)
        {
            //对方断开连接
             std::cout<<"["<<ClientFd<<"号客户端]断开连接,我也断开连接"<<std::endl;
             close(ClientFd);
             for(int i=1;i<NUM;i++)
             {
                if(ReadFdArray[i]==ClientFd)
                {
                    ReadFdArray[i]=-1;
                    break;
                }
             }
        }
        else{
            message(WARNING,"接收%d号客户端消息失败  errno:%d  错误原因:%s",ClientFd,errno,strerror(errno));
        }
    }

private:
    int ReadFdArray[NUM];
    fd_set readset;
    Socket listen_socket;
    std::string server_ip;
    std::string server_port;
};