#include <string>
#include <iostream>
#include <vector>
class Split{
public:
static void splitstring(const std::string s, std::string sep, std::vector<std::string> *out)
{
    int start = 0;
    while (start < s.size())
    {
        size_t pos = s.find(sep, start);
        std::string sub = pos == std::string::npos ? s.substr(start) : s.substr(start, pos - start);
        (*out).push_back(sub);
        if (pos == std::string::npos)
            break;
        start = pos + sep.size();
    }
}
};