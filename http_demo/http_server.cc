#include <iostream>
#include <fstream>
#include <vector>
#include <sys/types.h>
#include <sys/socket.h>
#include "server.hpp"
#include "Log.hpp"
#include "Usage.hpp"
#include "split.hpp"
#define ROOT "./wwwroot"
void server_task(int serversocket)
{
    char buff[10240];
    ssize_t reasize = recv(serversocket, buff, sizeof(buff) - 1, 0);
    if (reasize < 0)
    {
        message(ERROR, "服务器接收客户端信息失败");
        return;
    }
    else if (reasize > 0)
    {
        buff[reasize] = 0;
        // std::cout<<buff<<std::endl;
        std::vector<std::string> vline;
        Split::splitstring(buff, "\r\n", &vline);
        /* for(auto&iter:vline)
        {
            std::cout<<"-------"<<iter<<std::endl;
        } */
        std::vector<std::string> vfirst;
        Split::splitstring(vline[0], " ", &vfirst);
        for (auto &iter : vfirst)
        {
            std::cout << "-------" << iter << std::endl;
        }
        std::string WebPath = vfirst[1] == "/" ? ("./wwwroot/index.html") : ROOT + vfirst[1];
        std::fstream target(WebPath, std::fstream::in);
        std::string buffer;
        if (target.is_open())
        {
            while (target.good())
            {
                std::string s;
                std::getline(target, s);
                buffer += s;
                std::cout << s << std::endl;
            }
        }
        std::string ret;
        ret += "http1.1";
        ret += " ";
        if (buffer.empty())
        {
            ret += "505";
            ret += " ";
            ret += "Error Not Found";
        }
        else
        {
            ret += "200";
            ret += " ";
            ret += "success";
        }

        ret += "\r\n";
        ret += "\r\n";
        ret += buffer;
        send(serversocket, ret.c_str(), ret.size(), 0);
    }
}
int main(int argc, const char *argv[])
{
    if (argc != 2)
    {
        Usage();
        exit(-1);
    }
    Server serve(argv[1], server_task);
    serve.start();
    return 0;
}