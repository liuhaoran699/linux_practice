#include<string>
#include<functional>
#include<signal.h>
#include"Socket.hpp"
class Server{
public:
using func_=std::function<void(int)>;
public:
Server(std::string port,func_ func,std::string ip="0.0.0.0"):serverport(port),serverip(ip),task(func)
{
    listensocket.Bind(serverport,serverip);
    listensocket.Listen();
}
void start()
{
    signal(SIGCHLD,SIG_IGN);
    while(1)
    {
        int _accept=listensocket.Accept(nullptr,nullptr);
        if(_accept==-1)
        {
            //接收连接失败
            continue;
        }
        //接收连接成功
        if(!fork())
        {
            //子进程处理连接请求
            close(listensocket.socket_);
            task(_accept);
            exit(0);
        }
        //父进程关闭用以处理连接请求的套接字
        close(_accept);
    }
}

private:
func_ task;
Socket listensocket;
std::string serverip;
std::string serverport;
};